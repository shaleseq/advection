
Benchmarks dla FVM i FDM zostały stworzone w oparciu o porównania.

1D
a) rozwiązanie analityczne z rozwiązaniem FDM zgodność ~ 1e-16

2D
a) 2D FDM z 1D FDM dla jednej z v_i = 0 rozwiązanie powinno zgadzać się(~1e-16) z rozwiązaniem 1D 
b) siatki niestrukturalne (FVM) w 2D można porównać z siatkami strukturalnymi (FDM). Warto pamiętać, aby powierzcznie elementów się zgadzamy, oraz o przeprowadzeniu interpolacji z węzłów niestrukturalnych do strukturalnych zgodność()

3D
a) 3D FDM z 1D FDM
b) siatki niestrukturalne z siatkami strukturalnymi
