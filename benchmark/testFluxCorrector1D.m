function testFluxCorrector1D
clear;
addpath('../msource/1D');
vel=1;
%% DOMAIN
x0 = -2*pi;
xf = 2 * pi;
t0 = 0;
tf = 2;
M  = 100;  % space
N  = tf*vel/(xf-x0)*M*1.5; % time
x  = linspace(x0,xf,M+1);
x(end) =[];
dx=x(2)-x(1);
t  = linspace(t0, tf, N+1);
    function y=genY(t)
        y  = zeros(1,M);
        L = xf - x0;
        dl = L/4;
        c = t*vel;
        y(x > c-dl & x < c+dl) = 1;
        d = c+dl-xf;
        y(x < x0+d) = 1;
    end
y=genY(0);
%% NUMERIC

uL =  advection(x, y, t, vel, 'lax');
vL =  fluxCorrectorAdvection(x, y, t, vel, 'laxC');

uLW = advection(x,y,t,vel,'laxwen');

uU =  advection(x, y, t, vel, 'upwind');
vU =  fluxCorrectorAdvection(x, y, t, vel, 'upwindC');

%% PLOTING
y=genY(t(end));
figure(1);
clf;
hold on;
axis([-8,8,-0.5,1.5]);
plot(x,y,'g-');
plot(x, uL(:,end), x, uLW(:, end),x, vL(:,end));
legend('EXACT', 'LAX','LAXWEN','LAX-LAXWEN');

figure(2);
clf;
hold on;
axis([-8,8,-0.5,1.5]);
plot(x,y,'g-');
plot(x, uU(:,end), x, vU(:, end));
legend('EXACT', 'UPWIND', 'UPWIND-LAXWEN');


end
