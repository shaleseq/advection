function testStrucUnstruc2D
clear
addpath('../msource');
addpath('../msource/2D');
addpath('../msource/1D');
addpath('../mex');
addpath('/home/repos/milamin/MILAMIN_v2/trunk/src/');
milamin_init(2,4)
%% DOMAIN
Mx=100;
My=100;

x0=0;
xf=1;
y0=0;
yf=1;

x=linspace(x0,xf,Mx+1);
y=linspace(y0,yf,My+1);
[X,Y]=meshgrid(x,y);

t0=0;
tf=1.0;


sx=0.25;
sy=0.25;

%% Functions
r=@(x,y) min(ones(size(x)), 4*sqrt((x-sx).^2+(y-sy).^2));

vx=@(x,y,t) sin(pi*x).^2.*sin(2*pi*y)*cos(pi*t/5);
vy=@(x,y,t) -sin(pi*y).^2.*sin(2*pi*x)*cos(pi*t/5);
f=@(x,y) 0.5*(1+cos(pi*r(x,y)));

%N=tf*(max(max(abs(vx(X,Y,t0))))+max(max(abs(vy(X,Y,t0)))))/(xf-x0)*(Mx+My);
%Nt=N+1;
dx = x(2)-x(1);
Nt=tf/dx*2;
t=linspace(t0,tf,Nt);
dt = t(2)-t(1);
%% FIELD
T=f(X,Y);
vxx = vx(X,Y,0);
vyy = vy(X,Y, 0);


%% Unstructured MESH

points   = [x0 y0; xf y0; xf yf; x0 yf]'; % corner points
segments = [1 2; 2 3; 3 4; 4 1]'; % segments połączenia


% Set triangle options
opts = [];
opts.element_type     = 'tri3';   % element type
opts.gen_neighbors    = 0;        % generate element neighbors
opts.triangulate_poly = 1;
opts.min_angle        = 30;
opts.gen_edges        = 1;
ar = (xf-x0)*(yf-y0)/(Mx*My);
opts.max_tri_area     = ar;


% Create triangle input structure
tristr.points         = points;
tristr.segments       = uint32(segments);  % note segments have to be uint32

MESH = mtriangle(opts, tristr);
MESH2 = MESH;
tic; MESH=prepareMeshElems(MESH);toc
tic; MESH2=prepareMeshNodes(MESH2);toc
MESH2.NEIGHBOURS = MESH2.ELEMS([2 3 1]',:);
vxt=squeeze(vx(MESH.SEGMENTS(1,:,:), MESH.SEGMENTS(2,:,:),0));
vyt=squeeze(vy(MESH.SEGMENTS(1,:,:), MESH.SEGMENTS(2,:,:),0));

vxt2=squeeze(vx(MESH2.SEGMENTS(1,:,:), MESH2.SEGMENTS(2,:,:),0));
vyt2=squeeze(vy(MESH2.SEGMENTS(1,:,:), MESH2.SEGMENTS(2,:,:),0));

MESH.r = (dt./MESH.AREA)';
MESH.idx0 = int32(repmat([1:length(MESH.ELEMS)],3,1));  

%% NUMERIC
u = fluxLimiterAdvection2D(x, y,T, t, vxx,vyy, 'upwind','superbee');
numThreads=4;
T = f(MESH.CENTER(1,:), MESH.CENTER(2, :));
Tn2 = f(MESH2.NODES(1,:), MESH2.NODES(2,:));
u2 = u;
u3 = u;
WS=[];
opts = [];
TR = [];
markers = [X(:)';Y(:)'];
for i = 1:length(t);
    T=advectionCElems(MESH.Nx, MESH.Ny, MESH.AREA, MESH.NEIGHBOURS, T, vxt,vyt, dt, numThreads);
    Tn2=advectionCNodes(MESH2.Nx, MESH2.Ny, MESH2.AREA, MESH2.ELEMS, MESH2.NEIGHBOURS,...
        Tn2, vxt2,vyt2, dt, numThreads);
    Tn1=toNodes(MESH,T);
    [TR, WS] = tsearch2(MESH.NODES, MESH.ELEMS(1:3, :),markers, WS, TR, opts);
    Tv1 = einterp(MESH, Tn1', markers, TR, opts);
    u2(:,:,i) = reshape(Tv1, My+1,Mx+1);
    
    [TR, WS] = tsearch2(MESH2.NODES, MESH2.ELEMS(1:3, :),markers, WS, TR, opts);
    Tv2 = einterp(MESH, Tn2, markers, TR, opts);
    u3(:,:,i) = reshape(Tv2, My+1,Mx+1);
end

%% PLOTS
figure(1)
clf;
surf(X,Y, u(:,:,end));
view(2);
axis equal,axis tight;
shading interp;
colorbar;
caxis([0,1]);

figure(2)
clf;
trisurf(MESH.ELEMS', MESH.NODES(1,:), MESH.NODES(2,:), Tn1);
view(2);
axis equal,axis tight;
shading interp;
colorbar;
caxis([0,1]);

figure(3)
clf;
trisurf(MESH2.ELEMS', MESH2.NODES(1,:), MESH2.NODES(2,:), Tn2);
view(2);
axis equal,axis tight;
shading interp;
colorbar;
caxis([0,1]);


%% TEST
display('difference 2D struct vs 2D unstruct');
display(['upwind struct vs elems base ' num2str(mean(abs(u2(:)-u(:))))]);
display(['upwind struct vs nodes base ' num2str(mean(abs(u3(:)-u(:))))]);
display(['upwind elems base vs nodes base ' num2str(mean(abs(u2(:)-u3(:))))]);


return;
figure(3);
clf;
for i=1:7:N
surf(X,Y,u(:,:,i));
view(2);
shading interp;
axis([x0 xf y0 yf]);
axis equal,axis off;
caxis([0 1]);
set(gcf,'Renderer','zbuffer');
colorbar
caxis([0 1]);
drawnow
pause(0.01);
end
idx = 1:length(u(1,1,:));
val = 0*idx;
for i=1:length(val)
    A=max(u(:,:,i));
    val(i) = max(A(:));
end
figure(4);
clf;
plot(idx,val);
