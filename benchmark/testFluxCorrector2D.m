function testFluxCorrector2D
clear;
addpath('../msource/2D');
addpath('../msource/1D');
warning('this test do not pass, some problems')
w    = 2;
sigm = 1;
%% DOMAIN
x0=-2*pi;
xf=2*pi;
y0=-2*pi;
yf=2*pi;
    
t0=0;
tf=10.0;
Mx=50;  % space
My=50;
x=linspace(x0,xf,Mx+1);
y=linspace(y0,yf,My+1);
dx = x(2)-x(1);
N=tf/dx*4;

x(end)=[];
y(end)=[];

[X,Y] = meshgrid(x,y);

t  = linspace(t0, tf, N+1);
%z = f(X,Y,sigm,w);
z=g(X,Y);
vx = ones(size(X));
vy = ones(size(Y));
%%NUMERIC
%u = fluxCorrectorAdvection2D(x, y,z, t, vy,vx, 'upwindC');

% For Test
NT=30;
vy = zeros(size(Y));
uD2 = fluxCorrectorAdvection2D(x, y,z, t, vy,vx, 'upwindC');
uD1 = fluxCorrectorAdvection(x, z(NT,:), t, vx(NT,:), 'upwindC');
display('difference 2D vs 1D');
display(['upwind corected ' num2str(max(abs(uD2(NT,:,end)'-uD1(:,end))))]);

return;
%% Ploting
h=figure(1);
clf
colorbar
axis tight
set(gcf,'Renderer','zbuffer');
for ii=1:8:N
surf(X,Y, u(:,:, ii));
view(2);
shading interp
axis equal
axis tight
caxis([min(u(:)) max(u(:))]);
colorbar;
pause(0.05);
end


%% Functions
function z=g(x,y)
        z=0*x;
        z(x>x0+(xf-x0)/3 & x<xf- (xf-x0)/3 & ...
            y>y0+(yf-y0)/3 & y<yf- (yf-y0)/3)=1;
end

function z=f(x,y,sigm,w)
    z = exp(-(x.^2+y.^2)/(2*sigm^2));
end

end
