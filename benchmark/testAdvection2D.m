function testAdvection2D
clear
addpath('../msource/2D');

% FUNCTION
w    = 2;
sigm = 1;
x0=-2*pi;
xf=2*pi;
y0=-2*pi;
yf=2*pi;
function z=f(x,y,sigm,w)
    z = exp(-(x.^2+y.^2)/(2*sigm^2)).*cos(w.*x);
end
    
%f = @(x,sigm,w) sin(x)+cos(x);

% PHYSICS


%% DOMAIN
t0=0;
tf=10;
Mx=100;  % space
My=100;
x=linspace(x0,xf,Mx+1);
y=linspace(y0,yf,My+1);

x(end)=[];
y(end)=[];

[X,Y] = meshgrid(x,y);

vx = ones(size(X));
vy = ones(size(Y));
dx = x(2)-x(1);
N=tf/dx*4; % time
t  = linspace(t0, tf, N+1);
%z  = f(X,Y,sigm,w);
z = g(X,Y);
% %% DISCRETIZATION
u = advection2D(x, y,z, t, vx,vy, 'upwind');
NT = 40;
%% For Test
vy = zeros(size(Y));
u1D2 = advection2D(x, y,z, t, vx,vy, 'upwind');
u2D2 = advection2D(x, y,z, t, vx,vy, 'laxwen');
u3D2 = advection2D(x, y,z, t, vx,vy, 'lax');
u4D2 = advection2D(x, y,z, t, vx,vy, 'middle');


u1D1 = advection(x, z(:,NT)', t, vx(:,1)', 'upwind');
u2D1 = advection(x, z(:,NT)', t, vx(:,1)', 'laxwen');
u3D1 = advection(x, z(:,NT)', t, vx(:,1)', 'lax');
u4D1 = advection(x, z(:,NT)', t, vx(:,1)', 'middle');


%% Test 2D vs 1D
%methods upwind, laxwen, lax, middle
display('difference 2D vs 1D');
display(['upwind ' num2str(max(abs(u1D2(NT,:,end)'-u1D1(:,end))))]);
display(['laxwen ' num2str(max(abs(u2D2(NT,:,end)'-u2D1(:,end))))]); 
display(['middle ' num2str(max(abs(u4D2(NT,:,end)'-u4D1(:,end))))]); 
display(['lax ' num2str(max(abs(u3D2(NT,:,end)'-u3D1(:,end))))]); 
warning('lax 2D is not lax in 1D because is more diffuse');


%% PLOTTING1
% Plot upwind
h=figure(1);
clf
colorbar
axis tight
set(gcf,'Renderer','zbuffer');
for ii=1:8:N
surf(X,Y, u(:,:, ii));
view(2);
shading interp
axis equal
axis tight
caxis([min(u(:)) max(u(:))]);
colorbar;
pause(0.05);
end

% h=figure(2);
% clf
% colorbar
% axis tight
% set(gcf,'Renderer','zbuffer');
% axis([x0 xf y0 yf -0.1 1.1]);
% caxis([min(u(:)) max(u(:))]);
% for ii=1:8:N
% surf(X,Y, u2(:,:, ii));
% view(2);
% shading interp
% pause(0.05);
% end



%% Functions
function z=g(x,y)
        z=0*x;
        z(x>x0+(xf-x0)/3 & x<xf- (xf-x0)/3 & ...
            y>y0+(yf-y0)/3 & y<yf- (yf-y0)/3)=1;
end

end 
