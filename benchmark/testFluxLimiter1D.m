function testFluxLimiter1D
clear;
vel=1;
addpath('../msource/1D');
%% DOMAIN
x0 = -2*pi;
xf = 2 * pi;
t0 = 0;
tf = 2;
M  = 100;  % space
N  = tf*vel/(xf-x0)*M*1.5; % time
x  = linspace(x0,xf,M+1);
x(end) =[];
dx=x(2)-x(1);
t  = linspace(t0, tf, N+1);
    function y=genY(t)
        y  = zeros(1,M);
        L = xf - x0;
        dl = L/4;
        c = t*vel;
        y(x > c-dl & x < c+dl) = 1;
        d = c+dl-xf;
        y(x < x0+d) = 1;
    end
y=genY(0);
%% NUMERIC
u = advection(x, y, t, vel, 'middle');
v1 = advection(x, y, t, vel, 'laxwen');
v2 = advection(x, y, t, vel, 'upwind');
v3 = fluxLimiterAdvection(x, y, t, vel, 'laxwen', 'superbee');

%% PLOTING
y=genY(t(end));
% figure(1);
% clf;
% hold on;
% axis([-8,8,-0.5,1.5]);
% plot(x, u(:,end));
% plot(x,y,'r-');
% legend('MIDDLE', 'EXACT');
% 
% figure(2);
% clf;
% hold on;
% axis([-8,8,-0.5,1.5]);
% plot(x, v1(:,end));
% plot(x,y,'r-');
% legend('LAXWEN', 'EXACT');

figure(1);
clf;
hold on;
axis([-8,8,-0.5,1.5]);
plot(x,y,'g-');
plot(x, v2(:,end),x, v1(:,end), x, v3(:,end));

legend('EXACT', 'UPWIND','LAXWEN', 'LAXWEN FLUX');

% figure(2);
% clf;
% hold on;
% axis([-8,8,-0.5,1.5]);
% plot(x, v1(:,end), x, v3(:,end));
% plot(x,y,'r-');
% legend('LAXWEN', 'LAXWEN FLUX', 'EXACT');
end
