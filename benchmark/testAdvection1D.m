function testAdvection1D
clear
addpath('../msource/1D');

% FUNCTION
w    = 2;
sigm = 1;
f = @(x,sigm,w) exp(-x.^2/(2*sigm.^2)).*cos(w.*x);
%f = @(x,sigm,w) sin(x)+cos(x);

% PHYSICS
vel = 2;

%% DOMAIN
x0 = -2*pi;
xf = 2 * pi;
t0 = 0;
tf = 1;
M  = 100;  % space
N  = ceil(tf*vel/(xf-x0)*M); % time
n=10;(N-1);%number after we check analytics solution and numeric solution
x  = linspace(x0,xf,M+1);

x(end) =[];
t  = linspace(t0, tf, N+1);
y  = f(x,sigm,w);

%% DISCRETIZATION
% - middle
u_mid = advection(x, y, t, vel, 'middle');
% - Lax
u_lax = advection(x, y, t, vel, 'lax');
% - ftcs
u_ftcs = advection(x, y, t, vel, 'ftcs');

u_laxWen = advection(x,y,t,vel, 'laxwen');

u_upWind = advection(x,y,t,vel, 'upwind');

u_upWind2R = advection(x,y,t,vel, 'upwind2R');


%% ANALYTICS

% Lax
y_lax = advectionAnalytics( x, y, t, vel,n, 'lax');

% Middle
y_mid = advectionAnalytics( x, y, t, vel,n, 'middle');

% FTCS
y_ftcs = advectionAnalytics( x, y, t, vel,n, 'ftcs');

% LaxWen

y_laxWen =  advectionAnalytics( x, y, t, vel,n, 'laxwen');

% UpWind
y_upWind = advectionAnalytics( x, y, t, vel,n, 'upwind');

% UpWind2R
y_upWind2R = advectionAnalytics( x, y, t, vel,n, 'upwind2R');


%% PLOTTING1
figure(1);
clf;
for ii = n+1:n+1%round(linspace(1,N,5))
    %ii = n+1;
    subplot(3,1,1);
    hold on
    x_t = bc(x-vel*t(ii),x0,xf);
    y_t = f(x_t, sigm, w);
    plot(x, y_t, 'r.');
    plot(x, u_mid(:, ii), 'b');
    legend('EXACT','MIDDLE',2);
    title(['function after ' num2str(n) ' steps']);
    
    subplot(3,1,2);
    hold on
    plot(x, f(bc(x-vel*t(ii), x0, xf), sigm, w), 'r.');
    plot(x, u_lax(:, ii), 'b');
    legend('EXACT','LAX',2);
    
    subplot(3,1,3);
    hold on
    plot(x, f(bc(x-vel*t(ii), x0, xf), sigm, w), 'r.');
    plot(x, u_ftcs(:, ii), 'b');
    legend('EXACT','FTCS',2);
end
figure(2)
clf
for ii = n+1:n+1%round(linspace(1,N,5))
    %ii = n+1;
    subplot(3,1,1);
    hold on
    plot(x, f(bc(x-vel*t(ii), x0, xf), sigm, w), 'r.');
    plot(x, u_laxWen(:, ii), 'b');
    legend('EXACT','LAX-WEN',2);
    title(['function after ' num2str(n) ' steps']);
    
    subplot(3,1,2);
    hold on
    plot(x, f(bc(x-vel*t(ii), x0, xf), sigm, w), 'r.');
    plot(x, u_upWind(:, ii), 'b');
    legend('EXACT','UPWiIND',2);
    
    subplot(3,1,3);
    hold on
    plot(x, f(bc(x-vel*t(ii), x0, xf), sigm, w), 'r.');
    plot(x, u_upWind2R(:, ii), 'b');
    legend('EXACT','UPWIND2R',2);
    
end

%% PLOTTING2
figure(3)
clf
subplot(3,1,1);
hold on
plot(x, u_lax(:, n+1)' -y_lax, 'b');
legend('Lax Error',2);
title(['error after ' num2str(n) ' steps, compare analytic/numeric']);    

subplot(3,1,2);
hold on
plot(x, u_mid(:, n+1)' -y_mid, 'b');
legend('Middle Error',2);%first step is lax, this is the reason of error;
    
subplot(3,1,3);
hold on
plot(x, u_ftcs(:, n+1)' -y_ftcs, 'b');
legend('Ftcs Error',2);

figure(4)
clf   
subplot(3,1,1);
hold on
plot(x, u_laxWen(:, n+1)' -y_laxWen, 'b');
legend('Lax-Wen Error',2);
title(['error after ' num2str(n) ' steps, compare analytic/numeric']); 

subplot(3,1,2);
hold on
plot(x, u_upWind(:, n+1)' -y_upWind, 'b');
legend('UpWind Error',2);
    
subplot(3,1,3);
hold on
plot(x, u_upWind2R(:, n+1)' -y_upWind2R, 'b');
legend('UpWind2R Error',2);

%% PLOTTING3
figure(5)
clf  
ii = n;
subplot(3,1,1);
hold on
x_t = bc(x-vel*t(ii),x0,xf);
y_t = f(x_t, sigm, w);
plot(x, y_t'-u_mid(:, ii), 'b');
legend('MIDDLE Error',2);
title(['error after ' num2str(n) ' steps, compare exact/numeric']);  

subplot(3,1,2);
hold on
plot(x, f(bc(x-vel*t(ii), x0, xf), sigm, w)'-u_lax(:, ii), 'b');
legend('LAX Error',2);

subplot(3,1,3);
hold on
plot(x, f(bc(x-vel*t(ii), x0, xf), sigm, w)' -u_ftcs(:, ii), 'b');
legend('FTCS Error',2);

figure(6)    
clf
subplot(3,1,1);
hold on
plot(x, f(bc(x-vel*t(ii), x0, xf), sigm, w)' -u_laxWen(:, ii), 'b');
legend('LAX-WEN Error',2);
title(['error after ' num2str(n) ' steps, compare exact/numeric']);

subplot(3,1,2);
hold on
plot(x, f(bc(x-vel*t(ii), x0, xf), sigm, w)'-u_upWind(:, ii), 'b');
legend('UPWiIND Error',2);

subplot(3,1,3);
hold on
plot(x, f(bc(x-vel*t(ii), x0, xf), sigm, w)' -u_upWind2R(:, ii), 'b');
legend('UPWIND2R Error',2);
end
    