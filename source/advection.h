#include <cstddef>


void advectionElems(double *Nx,double *Ny, double *Area, double *T, double *vx, double *vy, double *newT,
					unsigned *Neighbours, double dt, size_t N, int numThreads);


void advectionNodes(double *Nx,double *Ny, double *Area, double *T, double *vx, double *vy, double *newT,
					unsigned *Elems, unsigned *Neighbours, double dt, size_t N, size_t Nv, int numThreads);


void advectionElems3D(double *Nx, double *Ny, double *Nz, double *Vol, double *T, double *vx, double *vy, double *vz, double *newT,
					unsigned *Neighbours, double dt, size_t N, int numThreads);


void advectionSegments(double *Nx,double *Ny, double *Area, double *T, double *vx, double *vy, double *newT,
					unsigned *ED2EL, double dt, size_t N, size_t Nl );
