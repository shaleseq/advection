#include <omp.h>
#include"advection.h"
#include <stdio.h>
#include <vector>

const int BIDX = 1;

void advectionElems(double *Nx,double *Ny, double *Area, double *T, double *vx, double *vy, double *newT,
					unsigned *Neighbours, double dt, size_t N , int numThreads)
{
	omp_set_num_threads(numThreads);	
	double flux;
	double dot_pro;
	unsigned i, j;
	#pragma omp parallel for \
		default(none) shared(newT, T, vx, vy, Nx, Ny, Neighbours, dt, Area, N) \
				private(i, j, flux, dot_pro) schedule(static)
	for(i = 0; i < N; i++)
	{
		flux = 0;
		for(j = 0; j < 3; j++)
		{
			dot_pro = vx[3*i+j]*Nx[3*i+j]+vy[3*i+j]*Ny[3*i+j];
			if(dot_pro < 0)
			{
				flux = flux+T[Neighbours[3*i+j]-BIDX]*dot_pro;
			}
			else
			{
				flux = flux + T[i]*dot_pro;
			}
		}
		newT[i] = T[i] - dt/Area[i]*flux;
	}
}

void advectionElems3D(double *Nx, double *Ny, double *Nz, double *Vol, double *T, double *vx, double *vy, double *vz, double *newT,
                    unsigned *Neighbours, double dt, size_t N, int numThreads)
{

	omp_set_num_threads(numThreads);	
	double flux;
	double dot_pro;
	unsigned i, j;
	#pragma omp parallel for \
		default(none) shared(newT, T, vx, vy,vz, Nx, Ny,Nz, Neighbours, dt, Vol, N) \
				private(i, j, flux, dot_pro) schedule(static)
	for(i = 0; i < N; i++)
	{
		flux = 0;
		for(j = 0; j < 4; j++)
		{
			dot_pro = vx[4*i+j]*Nx[4*i+j]+vy[4*i+j]*Ny[4*i+j]+vz[4*i+j]*Nz[4*i+j];
			if(dot_pro < 0)
			{
				flux = flux+T[Neighbours[4*i+j]-BIDX]*dot_pro;
			}
			else
			{
				flux = flux + T[i]*dot_pro;
			}
		}
		newT[i] = T[i] - dt/Vol[i]*flux;
	}
}


void advectionSegments(double *Nx,double *Ny, double *Area, double *T, double *vx, double *vy, double *newT,
					unsigned *ED2EL, double dt, size_t N, size_t Nl )
{

	double dot_pro, flux;
	unsigned i;
	Nl /= 2;


	#pragma omp parallel for \
		default(none) shared(newT, T, N) \
				private(i) schedule(static)
	for(i = 0; i < N; i++)
	{
		newT[i] = T[i];
	}

	for(i = 0; i < Nl; i++)
	{
		dot_pro = vx[i]*Nx[i] + vy[i]*Ny[i];
		if(dot_pro > 0)
		{
			flux = dot_pro * T[ED2EL[i]-BIDX];
			newT[ED2EL[i]-BIDX] = newT[ED2EL[i]-BIDX] - dt / Area[ED2EL[i]-BIDX]*flux;
			newT[ED2EL[i+Nl]-BIDX] = newT[ED2EL[i+Nl]-BIDX] + dt / Area[ED2EL[i+Nl]-BIDX]*flux;
		}
		if(dot_pro < 0)
		{
			flux =  dot_pro * T[ED2EL[i+Nl]-BIDX];
			newT[ED2EL[i]-BIDX] = newT[ED2EL[i]-BIDX] - dt / Area[ED2EL[i]-BIDX]*flux;
			newT[ED2EL[i+Nl]-BIDX] = newT[ED2EL[i+Nl]-BIDX] + dt / Area[ED2EL[i+Nl]-BIDX]*flux;
		}
		
	}
}


void advectionNodes(double *Nx,double *Ny, double *Area, double *T, double *vx, double *vy, double *newT,
					unsigned *Elems, unsigned* Neighbours, double dt, size_t N, size_t Nv, int numThreads)
{
	omp_set_num_threads(numThreads);
	std::vector<double*> localT(numThreads);	
	double dot_pro;
	#pragma omp parallel default (none) \
		private(dot_pro) shared(N, Nv, Nx, Ny, Area, T, vx, vy, newT, Elems, Neighbours, dt, numThreads, localT)
	{
	double* myT = new double[Nv]{};
	localT[omp_get_thread_num()] = myT;

	#pragma omp for
	for(unsigned i = 0; i < N; i++)
	{
		for(unsigned j = 0; j < 3; j++)
		{
			
			dot_pro = Nx[i*3+j]*vx[i*3+j] + Ny[i*3+j]*vy[i*3+j];
			if(dot_pro < 0)
				dot_pro *= T[Neighbours[i*3+j]-BIDX];
			else
				dot_pro *= T[Elems[i*3+j]-BIDX];
			
			myT[Elems[i*3+j]-BIDX] -= dot_pro * dt / Area[Elems[i*3+j]-BIDX];
			myT[Neighbours[i*3+j]-BIDX] += dot_pro * dt / Area[Neighbours[i*3+j]-BIDX];
			
		}
	}
	#pragma omp for
	for(unsigned i = 0; i < Nv; i++)
	{
		for(int j = 0; j < numThreads; j++)
		{
			newT[i] += localT[j][i];
		}
	}
		delete[] myT;
	}
}
