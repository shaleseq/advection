#include "mex.h"
#include "../source/advection.h"
#include <string.h>
#include <stdio.h>

void mexFunction(int nlhs, mxArray* plhs[],
        int nrhs, const mxArray* prhs[])
{
    //C MESH PROPERTIES
    double* Nx,*Ny,*Area,*T,*vx,*vy, *newT;
	unsigned *Elems, *Neighbours;
	double dt;
	
	size_t N, Nv;
    
    /* check for proper number of arguments */
    if(nrhs < 9) {
        mexErrMsgIdAndTxt("MyToolbox:advectionCElems:nrhs","at least 8 inputs required.");
    }
    if(nlhs!=1) {
        mexErrMsgIdAndTxt("MyToolbox:advectionCElems:nlhs","1 output required.");
	}

	Nx = mxGetPr(prhs[0]);
	Ny = mxGetPr(prhs[1]);
	Area = mxGetPr(prhs[2]);
	Elems = (unsigned*)mxGetPr(prhs[3]);
	Neighbours = (unsigned*)mxGetPr(prhs[4]);
	T = mxGetPr(prhs[5]);
	vx = mxGetPr(prhs[6]);
	vy = mxGetPr(prhs[7]);	
    dt = mxGetScalar(prhs[8]);

	int numThreads = 4;
	if(nrhs == 10)
		numThreads = mxGetScalar(prhs[9]);

	N = mxGetN(prhs[4]);
	Nv = mxGetN(prhs[5]);
	
	//mexPrintf("Size= %d %d\n", N, Nv);
	plhs[0] = mxCreateDoubleMatrix(1,Nv, mxREAL);
	newT = mxGetPr(plhs[0]);
	memcpy(newT, T, sizeof(double)*Nv);
	//printf("Size= %d %d\n", N, Nv);
	//Run solver
	advectionNodes(Nx, Ny, Area, T, vx, vy, newT, Elems, Neighbours, dt, N,Nv, numThreads);
}
