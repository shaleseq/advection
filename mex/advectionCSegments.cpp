#include "mex.h"
#include "../source/advection.h"


void mexFunction(int nlhs, mxArray* plhs[],
        int nrhs, const mxArray* prhs[])
{
    //C MESH PROPERTIES
    double* Nx,*Ny,*Area,*T,*vx,*vy, *newT;
	unsigned* ED2EL;
	double dt;
	
	size_t N,Nl;
    
    /* check for proper number of arguments */
    if(nrhs!=8) {
        mexErrMsgIdAndTxt("MyToolbox:advectionCElems:nrhs","8 inputs required.");
    }
    if(nlhs!=1) {
        mexErrMsgIdAndTxt("MyToolbox:advectionCElems:nlhs","1 output required.");
	}

	Nx = mxGetPr(prhs[0]);
	Ny = mxGetPr(prhs[1]);
	Area = mxGetPr(prhs[2]);
	ED2EL = (unsigned*)mxGetPr(prhs[3]);
	T = mxGetPr(prhs[4]);
	vx = mxGetPr(prhs[5]);
	vy = mxGetPr(prhs[6]);	
    dt = mxGetScalar(prhs[7]);

	N = mxGetN(prhs[4]);
	Nl = mxGetN(prhs[3]);

	plhs[0] = mxCreateDoubleMatrix(1,N, mxREAL);
	newT = mxGetPr(plhs[0]);
	
	//Run solver
	advectionSegments(Nx, Ny, Area, T, vx, vy, newT, ED2EL, dt, N, Nl);
}
