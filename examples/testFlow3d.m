function testFlow3d
%show advection solvers for simple case
clear;
addpath('../msource');
addpath('../msource/ODE');
addpath('../msource/geometry');

%% DOMAIN
N = 50;
Nh = 200;
w = 2;
x0 = 0;
xf = 1;
y0 = 0;
yf = 1;
z0 = 0;
zf = 10;
Lz = zf-z0;

R=1;
S=9;
H=1;
x = linspace(x0, xf, N);
y = linspace(y0, yf, N);
%% Functions
hpf = @(x,y) (sin(w*x).*cos(w*x) + sin(w*y).*cos(w*y))*0 + H;
hnf = @(x,y) (sin(w*x)+cos(w*x)) .* (sin(w*y)+cos(w*y)) ./4 *0;

%PLC = genCube(x0,xf,y0,yf,N,hpf, hnf);
PLC = genCylinder(1, 10, 30, 30);
%saveStlText(PLC.facetlist, PLC.pointlist, 'cylinder.stl');

opt = 'pVq1.2a0.001';
[MESH, VORO] = tetgen(opt, PLC, [], [], []);
figure(1); clf;
trisurf(MESH.FACES',MESH.NODES(1,:), MESH.NODES(2,:), MESH.NODES(3,:), zeros(1, size(MESH.NODES, 2)));
axis equal, axis tight;

%saveStlText(tri, points, 'cube.stl');

Xp = reshape(MESH.NODES(1, MESH.EDGES), 2, []);
Yp = reshape(MESH.NODES(2, MESH.EDGES), 2, []);
Zp = reshape(MESH.NODES(3, MESH.EDGES), 2, []);



%% Analytic solution for velocity

%velocity = @(x,y,z) [zeros(size(x));zeros(size(y));0.5-(x-0.5).^2-(y-0.5).^2)];
vel = velocity(MESH.NODES(1,:), MESH.NODES(2,:), MESH.NODES(3,:));
%% Advection

opts = [];
WS = [];
% WS.xmin = x0;
% WS.xmax = xf;
% WS.ymin = y0;
% WS.ymax = yf;
% WS.zmin = z0;
% WS.zmax = zf;
%% MARKERS
nm = 1e5;
r = rand(3,nm);
r(1,:) = r(1,:)*R;
r(2,:) = r(2,:)*2*pi;
%r(3,:) = r(3,:)*-0.1+10;
r(3,:) = r(3,:)*pi;
xm = r(1,:).*cos(r(2,:)).*sin(r(3,:));
ym = r(1,:).*sin(r(2,:)).*sin(r(3,:));
zm = r(1,:).*cos(r(3,:))+S;
r=[xm;ym;zm];
MESH.ELEMS = uint32(MESH.ELEMS);

figure(2);
clf;
plot3(xm,ym,zm,'.')
patch(Xp, Yp, Zp, 'k');
axis equal, axis tight;

t0 =0;
tf = (7)/sqrt(max(dot(vel, vel)));
na = 10;
tp = (tf-t0) ./na;
Nt = 10;
t=linspace(t0, tf, na*Nt);
dt = t(2)-t(1);

for i=1:na
    r = odeAB2(r,0,tp,Nt,@vf);
    figure(3);
    clf;
    hold on
    plot3(r(3,:),r(2,:), r(1,:),'.');
    patch(Zp, Yp, Xp, 'k');
    axis equal, axis tight;
    view(3);
    pause(0.05);
end
%% Chain
plc = genSphere(4);
mesh.NODES=plc.pointlist;
mesh.ELEMS=plc.facetlist;
mesh.NODES(1, :) = mesh.NODES(1,:)*R;
mesh.NODES(2, :) = mesh.NODES(2,:)*R;
mesh.NODES(3, :) = mesh.NODES(3, :)+S;


figure(4);
clf;
hold on;
trisurf(mesh.ELEMS', mesh.NODES(1,:), mesh.NODES(2,:),mesh.NODES(3,:),zeros(1, length(mesh.NODES)));
patch(Xp, Yp, Zp, 'k');
axis equal, axis tight;
view(3);

r = [mesh.NODES(1,:); mesh.NODES(2,:); mesh.NODES(3,:)];
for i=1:na
    r = odeAB2(r,0,tp,Nt,@vf);
    figure(5);
    clf;
    hold on
    trisurf(mesh.ELEMS', r(3,:), r(2,:), r(1,:), zeros(1, length(mesh.NODES)));
    patch(Zp, Yp, Xp, 'k');
    axis equal, axis tight;
    view(3);
    pause(0.05);
end
%% FVM
% I have code elems base upwind for 3D case



%% CHARACTERICTIC
foot=MESH.NODES - dt*vel;

id1 = foot(3, :) < z0;
id2 = foot(3, :) > zf;
foot(3, id1) = foot(3, id1) + Lz;
foot(3, id2) = foot(3, id2) - Lz;

Tv = zeros(1, length(MESH.NODES));
Tv(sqrt(MESH.NODES(1,:).^2+MESH.NODES(2,:).^2+(MESH.NODES(3,:)-S).^2) <= R) = 1;
[TR, WS] = tsearch2(MESH.NODES, MESH.ELEMS, foot, WS, [], opts);
idx = TR ~= 0;
figure(6);
clf;
id = mean(reshape(Tv(MESH.ELEMS), 4, [])) ==1;
patch(reshape(MESH.NODES(1, MESH.ELEMS(:,id)),4,[]),reshape(MESH.NODES(2, MESH.ELEMS(:,id)),4,[]),...
    reshape(MESH.NODES(3, MESH.ELEMS(:,id)),4,[]), mean(reshape(Tv(MESH.ELEMS(:,id)),4,[])));
axis equal, axis tight
caxis([0, 1]);
colorbar;
view(3);

figure(7);clf;
for i=1:Nt*4
    [TR(idx), WS] = tsearch2(MESH.NODES, MESH.ELEMS, foot(:,idx), WS, TR(idx), opts);
    Tv(idx) = tinterp(MESH, Tv, foot(:,idx), TR(idx), opts, WS);
    if mod(i+1,4) == 0
        trisurf(MESH.ELEMS', MESH.NODES(3,:),MESH.NODES(2,:),MESH.NODES(1,:), Tv);
        view(3);
        shading interp;
        set(gcf,'Renderer','zbuffer');
        axis equal, axis tight
        caxis([0, 1]);
        colorbar;
        pause(0.01);
        
    end
    paraview_write(MESH.NODES, MESH.ELEMS, [], Tv, 'char/t',i);
end

%% FUNCTIONS
    function vel = velocity(x,y,z)
        vel=zeros(3, length(x));
        rr = sqrt(x.^2+y.^2);
        vel(3, :) = -(-(rr.^2)+1);
    end
    function v = vf(t,r)
        v = velocity(r(1,:), r(2,:), r(3,:));
    end

    
end