%Create simple fracture in 3D.
clear;
addpath('../msource');
%% DOMAIN
NN = 1000;
N = 50;
Nh = 10;
w = 2;
scale = 10;
x0 = -scale;
xf = scale;
y0 = -scale;
yf = scale;
x = linspace(x0, xf, N);
y = linspace(y0, yf, N);
H=5;
%% Functions
hpf = @(x,y) sin(w*x).*cos(w*x) + sin(w*y).*cos(w*y) + H;
hnf = @(x,y) (sin(w*x)+cos(w*x)) .* (sin(w*y)+cos(w*y)) ./4;

[X,Y] = meshgrid(x,y);
Zp = hpf(X,Y);
Zn = hnf(X,Y);

%% Noise

Zp = noise(N, -3)*2+H;
Zn = noise(N, -3)*2;

pointsp = [X(:)';Y(:)';Zp(:)'];
pointsn = [X(:)';Y(:)';Zn(:)'];
points = [pointsp pointsn];
M=length(pointsp);
tri = zeros(3, 2*(N-1)*(N-1));
for j=1:N-1
    tri(:,2*(j-1)*(N-1)+1) = [1+(j-1)*N;2+(j-1)*N;N+1+(j-1)*N];
    for i=2:N-1
        tri(:,2*((i-1)+(j-1)*(N-1))) = [i+(j-1)*N; i+1+(j-1)*N; i+j*N];
        tri(:,2*((i-1)+(j-1)*(N-1))+1) = [i+(j-1)*N; i+j*N; (i-1)+j*N];
    end
    tri(:,2*j*(N-1)) = [j*N;(j+1)*N;(N-1)+j*N];
end


% trib = zeros(3, (N-1)*8);
% 
% for j=1:N-1
%     trib(:,(j-1)*2+1) = [M+j;j;M+j+1];
%     trib(:,j*2) = [M+j+1;j;j+1];
%     
%     trib(:,(j-1)*2+1+2*(N-1)) = [(N-1)*N+j;M+(N-1)*N+j;M+(N-1)*N+j+1];
%     trib(:,j*2+2*(N-1)) = [(N-1)*N+j;M+(N-1)*N+j+1;(N-1)*N+j+1];
%     
%     trib(:,(j-1)*2+1+4*(N-1)) = [(j-1)*(N)+1; M+(j-1)*(N)+1;  M+(j)*(N)+1];
%     trib(:,j*2+4*(N-1)) = [ (j-1)*(N)+1; M+(j)*(N)+1; (j)*(N)+1];
%     
%     trib(:,(j-1)*2+1+6*(N-1)) = [N*j+M; N*j;  N*(j+1)+M];
%     trib(:,j*2+6*(N-1)) = [N*(j+1)+M; N*j;  N*(j+1)];
% end

%tri = [[tri(2,:); tri(1,:); tri(3,:)] tri+M trib];

tri = [[tri(2,:); tri(1,:); tri(3,:)] tri+M];
Ntri = length(tri);
facetlist = tri2plc(tri);

btri1 = [N:-1:1 M+1:M+N];
btri2 = [M:-1:M-N+1 2*M-N+1:2*M];
btri3 = [M-N+1:-N:1 M+1:N:2*M-N+1];
btri4 = [M:-N:N M+N:N:2*M];
btri5 = [1:M];
btri6 = [1+M:2*M];
facetlist(Ntri+1).polygonlist{1} = int32(btri1);
facetlist(Ntri+2).polygonlist{1} = int32(btri2);
facetlist(Ntri+3).polygonlist{1} = int32(btri3);
facetlist(Ntri+4).polygonlist{1} = int32(btri4);
% facetlist(5).polygonlist{1} = int32(btri5);
% facetlist(6).polygonlist{1} = int32(btri6);

facetmarkerlist = zeros(1,length(facetlist));
facetmarkerlist(1:Ntri/2)=1;
facetmarkerlist(Ntri/2+1:Ntri)=2;
facetmarkerlist(Ntri+1)=3;
facetmarkerlist(Ntri+2)=4;
facetmarkerlist(Ntri+3)=5;
facetmarkerlist(Ntri+4)=6;

PLC.pointlist = points;
PLC.facetlist = facetlist;
PLC.facetmarkerlist=int32(facetmarkerlist);

%% TEST
figure(2);
clf;
hold on;
plot3(points(1, btri1), points(2, btri1), points(3, btri1), '-');
plot3(points(1, btri2), points(2, btri2), points(3, btri2), '-');
plot3(points(1, btri3), points(2, btri3), points(3, btri3), '-');
plot3(points(1, btri4), points(2, btri4), points(3, btri4), '-');
view(3);
% plot3(points(1, btri5), points(2, btri6), points(3, btri5), '.');
% plot3(points(1, btri6), points(2, btri5), points(3, btri6), '.'); 
%saveStlText(tri, points, 'frac.stl');
%return;
opt1 = 'pVqq12a10';
opt2= 'pkVqq12a1';
opt3 = 'pkVqq12a0.1';
[MESH1, VORO] = tetgen(opt1, PLC, [], [], []);
% [MESH2, VORO] = tetgen(opt2, PLC, [], [], []);
% [MESH3, VORO] = tetgen(opt3, PLC, [], [], []);
%save('mesh3e5.mat','-struct', 'MESH1');
% save('mesh1e6.mat','-struct', 'MESH2');
% save('mesh5e6.mat','-struct', 'MESH3');
figure(1); clf;
trisurf(MESH1.FACES',MESH1.NODES(1,:), MESH1.NODES(2,:), MESH1.NODES(3,:), zeros(1, size(MESH1.NODES, 2)));
axis square;
