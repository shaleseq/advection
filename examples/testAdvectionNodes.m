clear
addpath('../mex');
addpath('../msource');
addpath('/home/repos/milamin/MILAMIN_v2/trunk/src/');
milamin_init(2,4)
%% DOMAIN
x0=0;
xf=1;
y0=0;
yf=1;

sx=0.25;
sy=0.25;

t0=0;
tf=1;
N=1000;
t=linspace(t0,tf,N);
%% MESH
points   = [x0 y0; xf y0; xf yf; x0 yf]'; % corner points
segments = [1 2; 2 3; 3 4; 4 1]'; % segments połączenia

%%
% Set triangle options
opts = [];
opts.element_type     = 'tri3';   % element type
opts.gen_neighbors    = 0;        % generate element neighbors
opts.triangulate_poly = 1;
opts.min_angle        = 30;
opts.gen_edges        = 1;
opts.max_tri_area     = 0.0001;

%%
% Create triangle input structure
tristr.points         = points;
tristr.segments       = uint32(segments);  % note segments have to be uint32


%% Functions
r=@(x,y) min(ones(size(x)), 4*sqrt((x-sx).^2+(y-sy).^2));
vx=@(x,y) sin(pi*x).^2.*sin(2*pi*y);
vy=@(x,y) -sin(pi*y).^2.*sin(2*pi*x);
f=@(x,y) 0.5*(1+cos(pi*r(x,y)));

%%
% Generate the mesh using triangle
MESH = mtriangle(opts, tristr);
tic; MESH=prepareMeshNodes(MESH);toc
%% MESH
% Show the mesh
ncorners = 3;
nel = length(MESH.ELEMS);
X = reshape(MESH.NODES(1,MESH.ELEMS(1:ncorners,:)), ncorners, nel);
Y = reshape(MESH.NODES(2,MESH.ELEMS(1:ncorners,:)), ncorners, nel);
T=f(MESH.NODES(1,:), MESH.NODES(2,:));
figure(1);clf;
h = patch(X, Y, 'g');
axis equal;

figure(2); clf;
trisurf(MESH.ELEMS',MESH.NODES(1,:),MESH.NODES(2,:),T);
view(2);
shading interp;
set(gcf,'Renderer','zbuffer');
axis tight
axis equal;
n=length(MESH.ELEMS);
m=length(MESH.NODES(:));
MESH.NEIGHBOURS = MESH.ELEMS([2 3 1]',:);

%% VALUES
vxt=squeeze(vx(MESH.SEGMENTS(1,:,:), MESH.SEGMENTS(2,:,:)));
vyt=squeeze(vy(MESH.SEGMENTS(1,:,:), MESH.SEGMENTS(2,:,:)));
dt=t(2)-t(1);
time = 0;
%% MESH TO PLOT
figure(3);clf;
for i=1:length(t)
    if mod(i,100) == 0
        trisurf(MESH.ELEMS',MESH.NODES(1,:),MESH.NODES(2,:),T);
        view(2);
        set(gcf,'Renderer','zbuffer');
        axis equal, axis tight
        colorbar;
        caxis([0 1]);
        pause(0.01);
        %break;
    end
    ti=tic;
    T=advectionCNodes(MESH.Nx, MESH.Ny, MESH.AREA, MESH.ELEMS, MESH.NEIGHBOURS, T, vxt,vyt, dt, 4);
    %T = advectionMNodes(MESH,T,dt, vxt,vyt, 'upwind');
    time = time + toc(ti);
end
display(['solver time = ' num2str(time)]);
display(['system size = ' num2str(nel) ' ' num2str(N)]);
display(['diffuse = ' num2str(max(T(:)))]);