function testFlow2D
%show advection solvers for simple case.
clear;
addpath('../msource');
addpath('../msource/ODE');
addpath('../mex');
addpath('/home/repos/milamin/MILAMIN_v2/trunk/src/');
milamin_init(2,4)
%% DOMAIN
N = 150;
Nh = 50;
w = 2;
x0 = 0;
xf = 2*pi;
x = linspace(x0, xf, N);
H=2;
%% Functions
hpf = @(x) sin(w*x).*cos(w*x) + H;
hnf = @(x) (sin(w*x)+cos(w*x))./2;


% hpf = @(x) rand(size(x)) + H;
% hnf = @(x) rand(size(x));

hp = hpf(x)*0+H;
hn = hnf(x)*0+1;

% hp = noise(N,-3)+H;
% hn = noise(N,-3);
% hp = hp(1,:);
% hn = hn(1, :);
% hp(end)=hp(1);
% hn(end)=hn(1);

y0 = min(hn);
yf = max(hp);

dx =  x(2)-x(1);
dy = (hp(1)-hn(1))/(Nh+1);
%% MESH
pb = [ones(1, Nh)*x(1), ones(1,Nh)*x(end);linspace(1,Nh, Nh)*dy+hn(1), linspace(1,Nh, Nh)*dy+hn(1)];
points   = [x x; hp hn]; % corner points
points = [points pb];

n1 = [1:N-1];
n2 = [2:N];
n3 = [N+1:2*N-1];
n4 = [N+2:2*N];

n5 = [2*N+1: 2*N+Nh-1];
n6 = [2*N+2: 2*N+Nh];
n7 = [2*N+Nh+1: 2*N+2*Nh-1];
n8 = [2*N+Nh+2: 2*N+2*Nh];

segments = [n1 n3 n5 n7 1 2*N+Nh N 2*N+2*Nh;
    n2 n4 n6 n8 2*N+1 N+1 2*N+Nh+1 2*N]; % segments połączenia

segmentmarkers = [1*ones(size(n1)) 2*ones(size(n3)) 3*ones(size(n5))  4*ones(size(n7)) ...
    3 3 4 4];

%%
% Set triangle options
opts = [];
opts.element_type     = 'tri3';   % element type
opts.gen_neighbors    = 0;        % generate element neighbors
opts.triangulate_poly = 1;
opts.min_angle        = 15;
opts.gen_edges        = 1;
%opts.max_tri_area     = 0.26;
opts.max_tri_area     = 0.001;
opts.other_options = 'Y';

%%
% Create triangle input structure
tristr.points         = points;
tristr.segments       = uint32(segments);  % note segments have to be uint32
tristr.segmentmarkers = uint32(segmentmarkers);  % note segments have to be uint32
ns = length(tristr.segments);
%%
% Generate the mesh using triangle
MESH = mtriangle(opts, tristr);

ncorners = 3;
nel = length(MESH.ELEMS);
nno = length(MESH.NODES);
X = reshape(MESH.NODES(1,MESH.ELEMS(1:ncorners,:)), ncorners, nel);
Y = reshape(MESH.NODES(2,MESH.ELEMS(1:ncorners,:)), ncorners, nel);

Xp = reshape(tristr.points(1, tristr.segments(1:2,:)), 2,ns);
Yp = reshape(tristr.points(2, tristr.segments(1:2,:)), 2,ns);
figure(1);
clf;
patch(Xp, Yp, 'k');
axis equal;
view(2);

figure(2);
clf;
patch(X, Y, 'g');
axis equal, axis tight;
view(2);


% reorder
reorder = 'metis';
opts.reordering_type = reorder;
MSH = mesh_convert(MESH, 'tri7');
MSH = mesh_reorder(MSH,opts);
MESH = mesh_convert(MSH, 'tri3');
MESH=prepareMeshNodes(MESH, 0);

xmin = min(MSH.NODES(1,:));
xmax = max(MSH.NODES(1,:));
ymin = min(MSH.NODES(2,:));
ymax = max(MSH.NODES(2,:));
top = unique(MSH.SEGMENTS(:,MSH.segment_markers==1));
bottom = unique(MSH.SEGMENTS(:,MSH.segment_markers==2));
left = unique(MSH.SEGMENTS(:,MSH.segment_markers==3));
right = unique(MSH.SEGMENTS(:,MSH.segment_markers==4));

%% periodicity
eps = 1e-6;
% x direction
%L = find(abs(MSH.NODES(1,:)-xmin)<eps  );
%R = find(abs(MSH.NODES(1,:)-xmax)<eps  );

L = setdiff(left,top);
L = setdiff(L,bottom)';
R = setdiff(right,top);
R = setdiff(R,bottom)';

[~,ind] = sort(MSH.NODES(2,L));
indx_L = L(ind); %indx_L(1) = []; indx_L(end)=[];
[~,ind] = sort(MSH.NODES(2,R));
indx_R = R(ind); %indx_R(1) = []; indx_R(end)=[];
MSH.periodic_nodes.x = uint32([indx_L; indx_R]);


% % y direction
% B = find(abs(MSH.NODES(2,:)-ymin)<eps);
% T = find(abs(MSH.NODES(2,:)-ymax)<eps);
% [~,ind] = sort(MSH.NODES(1,B));
% indx_B = B(ind); %indx_B(1) = []; indx_T(end)=[];
% [~,ind] = sort(MSH.NODES(1,T));
% indx_T = T(ind); %indx_B(1) = []; indx_T(end)=[];
% MSH.periodic_nodes.y = uint32([indx_B; indx_T]);
MSH = mesh_set_periodicity(MSH, 'x');

%opts.periodic_show = 1;
%mesh_show(MSH, ones(1,MSH.info.nel)',opts)



% MSH info
[nnodel,nel]=size(MSH.ELEMS);
[ndim,nno]=size(MSH.NODES);
%% Stokes solver

% boundary conditions
BC = bc_init(MSH,2);
% inclusion rims - no slip, V=0
BC = bc_update('dirichlet',BC, [top bottom], 1, 0);
BC = bc_update('dirichlet',BC, [top bottom], 2, 0);

el_Mu = ones(MSH.info.nel,1);

H = src_init(MSH,1);
ind_all = 1:MSH.info.nel;
sf = @(x,y,t)1;
H = src_update(H,ind_all,1,sf);
H = src_eval(H, MSH);
rhs = zeros(MSH.info.ndim*MSH.info.nnod,1);
rhs(1:MSH.info.ndim:end) = H.val;


[Vel, p] = flow2d(MSH,BC,el_Mu,rhs);

%% Post-processing
Vm = sqrt(Vel(1,:).^2+Vel(2,:).^2);
figure(3);
clf;
mesh_show(MSH, Vm')

%% ADVECTION
%% MARKERS
WS = [];
nm = 1e4;
r = rand(1,nm);

m0 = 0
ml = (xf-x0)./40;
mf = m0 + ml;
xm = r*ml+m0;
idx = floor(r*N/40+1.5);
nf = max(idx);
n0 = min(idx);
ym = rand(1,nm).*(hp(idx)-hn(idx))+hn(idx);
[TR, WS] = tsearch2(MESH.NODES, MESH.ELEMS(1:3, :), [xm;ym], WS, [], opts);
xm(TR==0)=[];
ym(TR==0)=[];
[TR, WS] = tsearch2(MESH.NODES, MESH.ELEMS(1:3, :), [xm;ym], WS, [], opts);

figure(4);
clf;
plot(xm,ym, '.')
patch(Xp, Yp, 'k');
axis equal, axis tight;

t0 =0;
tf = (xf*0.7-x0)/max(Vel(1,:));
Nt = 1e2;
t=linspace(t0, tf, Nt);
dt=t(2)-t(1);
na = 10;
ts = tf/na;
nt = Nt/na;
r = [xm; ym];
    
for i=1:na
    r = odeAB2(r,0,ts,nt,@vf);
    figure(5);
    clf;
    hold on
    plot(r(1,:),r(2,:), '.');
    patch(Xp, Yp, 'k');
    axis equal, axis tight;
    pause(0.01);
end
%% Chain
nm=1e3;
xm = [ones(1, nm)*m0 ones(1, nm)*mf];
ym = [fliplr(linspace(hn(n0),hp(n0), nm)), linspace(hn(nf),hp(nf), nm)];
xm = [xm xm(1)];
ym = [ym ym(1)];
[TR, WS] = tsearch2(MESH.NODES, MESH.ELEMS(1:3, :), [xm;ym], WS, [], opts);
xm(TR==0)=[];
ym(TR==0)=[];
[TR, WS] = tsearch2(MESH.NODES, MESH.ELEMS(1:3, :), [xm;ym], WS, [], opts);

S=[0 cumsum(sqrt(diff(xm).^2+diff(ym).^2))];
L=S(end);
U=linspace(0,L,nm);
xm=interp1(S,xm,U);
ym=interp1(S,ym,U);
[TR, WS] = tsearch2(MESH.NODES, MESH.ELEMS(1:3, :), [xm;ym], WS, [], opts);

figure(6);
clf;
plot(xm,ym, '-b')
plot(xm,ym, '.r')
patch(Xp, Yp, 'k');
axis equal, axis tight;
r = [xm; ym];
for i=1:na
    r = odeAB2(r,0,ts,nt,@vf);
    S=[0 cumsum(sqrt(diff(r(1,:)).^2+diff(r(2,:)).^2))];
    L=S(end);
    U=linspace(0,L,nm);
    r(1,:)=interp1(S,r(1,:),U);
    r(2,:)=interp1(S,r(2,:),U);
    
    figure(7);
    clf;
    hold on
    plot(r(1,:),r(2,:), '-b');
    plot(r(1,:),r(2,:), '.r')
    patch(Xp, Yp, 'k');
    axis equal, axis tight;
    pause(0.01);
end

%% FVM
Nt=Nt*10;
dt=dt/10;
Tv=zeros(1,length(MESH.NODES));
Tv(MESH.NODES(1,:) <= mf & MESH.NODES(1,:) >= m0) = 1;
time = 0;
numThreads = 4;


[TR, WS] = tsearch2(MESH.NODES, MESH.ELEMS(1:3, :), MESH.SEGMENTS(1:2,:), WS, [], opts);
vel = vf(0, MESH.SEGMENTS(1:2,:));
vxt = reshape(vel(1, :),3,[]);
vyt = reshape(vel(2, :), 3, []);


% MESH TO PLOT
figure(8);clf;
for i=1:Nt
    if mod(i,uint32(Nt/na)) == 0 || i == 1
        %patch(reshape(MESH.NODES(1,MESH.ELEMS),3,[]), reshape(MESH.NODES(2,MESH.ELEMS),3,[]),Tv)
        trisurf(MESH.ELEMS', MESH.NODES(1,:),MESH.NODES(2,:),Tv)
        view(2);
        %set(gcf,'Renderer','zbuffer');
        axis equal, axis tight
        %axis([x0 xf y0 yf]);
        colorbar;
        caxis([0 1]);
        shading interp;
        pause(0.01);
    end
    ti=tic;
    Tv=advectionCNodes(MESH.Nx, MESH.Ny, MESH.AREA, MESH.ELEMS, MESH.NEIGHBOURS, Tv, vxt,vyt, dt, numThreads);
    time = time + toc(ti);
end
display(['solver time ' num2str(time)]);

figure(9);
clf;
[C,h]=tricontour(MESH.ELEMS',MESH.NODES(1,:),MESH.NODES(2,:),Tv, linspace(0, 1, 30));
view(2);
%axis([x0 xf y0 yf]);
patch(Xp, Yp, 'k');
axis equal, axis tight
colorbar;
caxis([0 1]);

%% Charakteristic
[TR, WS] = tsearch2(MESH.NODES, MESH.ELEMS(1:3, :), MESH.NODES, WS, [], opts);
vel = vf(0,MESH.NODES);
foot=MESH.NODES - dt*vel;
id1 = foot(1, :) < x0;
id2 = foot(1, :) > xf;
foot(1, id1) = foot(1, id1) + (xf-x0);
foot(1, id2) = foot(1, id2) - (xf-x0);
[TR, WS] = tsearch2(MESH.NODES, MESH.ELEMS(1:3, :), foot, WS, TR, opts);
Tc = zeros(1, length(MESH.NODES));
Tc(MESH.NODES(1,:) <= mf & MESH.NODES(1,:) >= m0 ) = 1;
figure(10);clf;
for i=1:Nt
    [TR, WS] = tsearch2(MESH.NODES, MESH.ELEMS(1:3, :), foot, WS, TR, opts);
    Tc = einterp(MESH, Tc, foot, TR, opts);
    if mod(i,uint32(Nt/na)) == 0 || i == 1
        trisurf(MESH.ELEMS(:,:)', MESH.NODES(1,:),MESH.NODES(2,:),Tc);
        view(2);
        %shading interp;
        %set(gcf,'Renderer','zbuffer');
        axis([x0 xf y0 yf]);
        axis equal, axis tight
        caxis([0, 1]);
        colorbar;
        shading interp;
        pause(0.01);
    end
end

figure(11);
clf;
[C,h]=tricontour(MESH.ELEMS(:,:)',MESH.NODES(1,:),MESH.NODES(2,:),Tc, linspace(0, 1, 30));
view(2);
patch(Xp, Yp, 'k');
axis equal, axis tight
colorbar;
caxis([0 1]);

%% CHECK

display('test upwind vs characteristic');
display(['difference = ' num2str(mean(abs(Tv-Tc)))]); 


%%FUNCTIONS

function v = vf(t,r)
        [TR, WS] = tsearch2(MESH.NODES, MESH.ELEMS(1:3, :), r, WS, TR, opts);
        idx = TR==0;
        TR(idx)=1; 
        vxx = einterp(MSH, Vel(1,:), r, TR, opts);
        vyy = einterp(MSH, Vel(2,:), r, TR, opts);
        v = [vxx;vyy];
        v(1,idx)=0;
        v(2, idx)=0;
    end

end



