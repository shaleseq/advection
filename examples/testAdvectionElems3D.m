function testAdvectionElems3D
clear
addpath('../mex');
addpath('../msource');
addpath('/home/polk/magisterka/ODE');
addpath('/home/polk/PGI/egu/3d');

%% DOMAIN
R=1;
S=9;
%% MESH
PLC = genCylinder(1, 10, 30, 30);

opt = 'pkVq1.3a0.0005';
[MESH, VORO] = tetgen(opt, PLC, [], [], 'log');
figure(1); clf;
trisurf(MESH.FACES',MESH.NODES(1,:), MESH.NODES(2,:), MESH.NODES(3,:), zeros(1, size(MESH.NODES, 2)));
axis equal, axis tight;

tic;
MESH=prepareMeshElems3D(MESH);
toc

Xp = reshape(MESH.NODES(1, MESH.EDGES), 2, []);
Yp = reshape(MESH.NODES(2, MESH.EDGES), 2, []);
Zp = reshape(MESH.NODES(3, MESH.EDGES), 2, []);


%% Analytic solution for velocity

%vel = velocity(MESH.NODES(1,:), MESH.NODES(2,:), MESH.NODES(3,:));
vel = velocity(MESH.FCENTER(1,:), MESH.FCENTER(2,:), MESH.FCENTER(3,:));
vx = reshape(vel(1,:), 4, []);
vy = reshape(vel(2,:), 4, []);
vz = reshape(vel(3,:), 4, []);

%% FVM

Tv = zeros(1, length(MESH.ELEMS));
Tv(sqrt(MESH.CENTER(1,:).^2+MESH.CENTER(2,:).^2+(MESH.CENTER(3,:)-S).^2) <= R) = 1;
figure(2);
clf;
patch(reshape(MESH.NODES(1, MESH.ELEMS),4,[]),reshape(MESH.NODES(2, MESH.ELEMS),4,[]),...
    reshape(MESH.NODES(3, MESH.ELEMS),4,[]), Tv);
axis equal, axis tight
caxis([0, 1]);
colorbar;
view(3);

t0 =0;
tf = (9)/sqrt(max(dot(vel, vel)));
Nt = 3000;
t=linspace(t0, tf, Nt);
dt = t(2)-t(1);
if (dt*max(vel(:))/min(MESH.VOL) > 1)
    display('unstable scheme\n');
    return;
end
nel=length(MESH.ELEMS);
MESH.idx0 = int32(repmat([1:nel],4,1));
method='upwind';
for i=1:Nt*2
    Tv=advectionMElems3D( MESH,Tv, dt, vx,vy,vz, method);
    if(~mod(i, 100) || i==1)
        paraview_write(MESH.NODES, MESH.ELEMS, [], Tv, 'data/t',i);
    end
end



%% FUNCTIONS
    function vel = velocity(x,y,z)
        vel=zeros(3, length(x));
        rr = sqrt(x.^2+y.^2);
        vel(3, :) = -(-(rr.^2)+1);
    end
    function v = vf(t,r)
        v = velocity(r(1,:), r(2,:), r(3,:));
    end

    
end