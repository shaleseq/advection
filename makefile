CC=gcc
CCFLAGS= -Wall -shared -fPIC -O2 -DNDEBUG
LIBS=-fopenmp
LIBFILE = ./source/advection.cpp
LIBOBJECT = libadvection.so
LINK= -ladvection
all:
	$(CC) $(LIBFILE) $(LIBS) $(CCFLAGS) -o $(LIBOBJECT)
clean:
	rm -rf $(LIBOBJECT)
