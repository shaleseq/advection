function [ T ] = advectionMNodes( MESH,T, dt, vx,vy, method)
if nargin < 6
    method =  'upwind';
end
switch method
    case 'upwind'
        T = advectionMNodesUpwind(MESH,T,dt,vx,vy);
    case 'laxwen'
       T = advectionMNodesLaxWen(MESH,T,dt,vx,vy);
    otherwise
           display('avaible methods upwind, laxwen');
           T=[];
end
end