function MESH = prepareMeshElems( MESH, reorder)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

if nargin < 2
    reorder = 1;
end
opts = [];
n=length(MESH.ELEMS);
%% REORDER
% Gx = mean(reshape(MESH.NODES(1,MESH.ELEMS),3,[]));
% Gy = mean(reshape(MESH.NODES(2,MESH.ELEMS),3,[]));
% NOD = [Gx; Gy];
% perm = geom_order(NOD);
% MESH.ELEMS = MESH.ELEMS(:,perm);

if reorder
reorder = 'metis';
opts.reordering_type = reorder;
MESH = mesh_reorder(MESH,opts);
end

%% NORMALS && CENTER
MESH.NORMALS = zeros(2, 3, n);
X = MESH.NODES(1, :);
Y = MESH.NODES(2, :);
idx = MESH.ELEMS;
lx1 = X(idx(2,:))-X(idx(1,:));
ly1 = Y(idx(2,:))-Y(idx(1,:));
lx2 = X(idx(3,:))-X(idx(2,:));
ly2 = Y(idx(3,:))-Y(idx(2,:));
lx3 = X(idx(1,:))-X(idx(3,:));
ly3 = Y(idx(1,:))-Y(idx(3,:));
MESH.NORMALS(:,1,:) = getNorm(lx1, ly1);
MESH.NORMALS(:,2,:) = getNorm(lx2, ly2);
MESH.NORMALS(:,3,:) = getNorm(lx3, ly3);

MESH.LENGTH = zeros(3,n);
MESH.LENGTH(1,:) = sqrt(lx1.^2+ly1.^2);
MESH.LENGTH(2,:) = sqrt(lx2.^2+ly2.^2);
MESH.LENGTH(3,:) = sqrt(lx3.^2+ly3.^2);

MESH.CENTER = zeros(2,n);
MESH.CENTER(1,:) = mean(reshape(MESH.NODES(1,MESH.ELEMS(1:3,:)),3,n));
MESH.CENTER(2,:) = mean(reshape(MESH.NODES(2,MESH.ELEMS(1:3,:)),3,n));
MESH.AREA = polyarea(reshape(MESH.NODES(1,MESH.ELEMS(1:3,:)),3,n),...
    reshape(MESH.NODES(2,MESH.ELEMS(1:3,:)),3,n));

%% SEGMENTS
MESH.SEGMENTS = zeros(2, 3, n);
MESH.SEGMENTS(:,1,:) = [(X(idx(2,:))+X(idx(1,:)))/2; (Y(idx(2,:))+Y(idx(1,:)))/2];
MESH.SEGMENTS(:,2,:) = [(X(idx(3,:))+X(idx(2,:)))/2; (Y(idx(3,:))+Y(idx(2,:)))/2];
MESH.SEGMENTS(:,3,:) = [(X(idx(1,:))+X(idx(3,:)))/2; (Y(idx(1,:))+Y(idx(3,:)))/2];

%% REORDER 2
EL = MESH.ELEMS;
nw = length(EL(1,:));
nh = length(EL(:,1));
EL2ED = zeros(1, nw*nh);
b=[2:nh 1];

ED = MESH.EDGES;
n=length(MESH.NODES(1,:));
nl = length(ED(1,:));
tED = uint64([ED ED([2,1],:)]);

tEL1 = reshape(EL, 1, nw*nh);
tEL2 = reshape(EL(b,:),1, nw*nh);
tEL = uint64([tEL1;tEL2]);

tED = tED(1,:)+(tED(2,:)-1)*n;
tEL = tEL(1,:)+(tEL(2,:)-1)*n;

[~,B,C]=intersect(tED,tEL);

EL2ED(C)=B;
EL2ED(EL2ED>nl)=-EL2ED(EL2ED>nl)+nl;
EL2ED=reshape(EL2ED, nh, nw);
EL2ED2 = EL2ED;
id = EL2ED < 0;
EL2ED2(id) = abs(EL2ED2(id))+nl;

ED2EL = zeros(1, nl*2);
ED2EL(EL2ED2) = reshape(repmat([1:nw], nh, 1),1,nh*nw);

neigh = EL2ED;
neigh(~id)=neigh(~id)+nl;
neigh=abs(neigh);
neigh=reshape(ED2EL(neigh(:)),nh,nw);
MESH.NEIGHBOURS=int32(neigh);
id = neigh ==0;
MESH.NORMALS(1,id) = 0;
MESH.NORMALS(2,id) = 0;

MESH.Nx = squeeze(MESH.NORMALS(1,:,:)).*MESH.LENGTH;
MESH.Ny = squeeze(MESH.NORMALS(2,:,:)).*MESH.LENGTH;

MESH.Lx = [lx1;lx2;lx3;];
MESH.Ly = [ly1;ly2;ly3;];
MESH.NX = squeeze(MESH.NORMALS(1,:,:));
MESH.NY = squeeze(MESH.NORMALS(2,:,:));
MESH.NX(isnan(MESH.NX))=0;
MESH.NY(isnan(MESH.NY))=0;
MESH.NX(isinf(MESH.NX))=0;
MESH.NY(isinf(MESH.NY))=0;

end

