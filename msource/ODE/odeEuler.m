function y = odeEuler(y0,t0,tf,n, v)
t=linspace(t0,tf,n);
Nt=length(t)-1;
dt=t(2)-t(1);
y=y0;
for i=1:Nt
    y=y+dt*v(t(i), y);
end
end

