function y = odeRK3(y0,t0,tf,n, v)
t=linspace(t0,tf,n);
Nt=length(t)-1;
dt=t(2)-t(1);
h=dt;
y=y0;
for i=1:Nt
    k1=v(t(i),y)*h;
    k2=v(t(i)+0.5*dt, y+0.5*k1)*h;
    k3=v(t(i)+h,y-k1+2*k2)*h;
    y=y+1/6*(k1+4*k2+k3);
end

end