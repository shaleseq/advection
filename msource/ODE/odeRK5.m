function y = odeRK5(y0,t0,tf,n, v)
t=linspace(t0,tf,n);
Nt=length(t)-1;
dt=t(2)-t(1);
h=dt;
y=y0;
for i=1:Nt
    k1=v(t(i),y)*h;
    k2=v(t(i)+0.25*h, y+0.25*k1)*h;
    k3=v(t(i)+0.25*dt, y+1/8*(k2+k1))*h;
    k4=v(t(i)+0.5*h,y-0.5*k2+k3)*h;
    k5=v(t(i)+0.75*h,y + 3/16*k1+9/16*k4)*h;
    k6=v(t(i)+h,y+ 1/7*(-3*k1+2*k2+12*k3-12*k4+8*k5))*h;
    y=y+1/90*(7*k1+32*k3+12*k4+32*k5+7*k6);
end
end