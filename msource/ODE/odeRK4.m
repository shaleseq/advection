function y = odeRK4(y0,t0,tf,n, v)
t=linspace(t0,tf,n);
Nt=length(t)-1;
dt=t(2)-t(1);
h=dt;
y=y0;
for i=1:Nt
    k1=v(t(i),y)*h;
    k2=v(t(i)+0.5*dt, y+0.5*k1)*h;
    k3=v(t(i)+0.5*dt, y+0.5*k2)*h;
    k4=v(t(i)+h, y+k3)*h;
    y=y+1/6*(k1+2*k2+2*k3+k4);
end
end