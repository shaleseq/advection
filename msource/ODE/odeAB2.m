function y = odeAB2(y0,t0,tf,n, v)
t=linspace(t0,tf,n);
Nt=length(t)-1;
dt=t(2)-t(1);
y = odeRK2(y0,t(1),t(2),2,v);
%y = odeEuler(y0,t(1),t(2),2,v);
F1 = v(t(1),y0);
F2 = v(t(2),y);
for i=2:Nt
    y=y+dt/2*(3*F2 -F1);
    F1=F2;
    F2=v(t(i+1),y);
end
end