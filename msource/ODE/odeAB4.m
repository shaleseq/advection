function y = odeAB4(y0,t0,tf,n, v)
t=linspace(t0,tf,n);
Nt=length(t)-1;
dt=t(2)-t(1);
y=y0;
F1 = v(t(1),y);
y = odeRK4(y,t(1),t(2),2,v);
F2 = v(t(2), y);
y = odeRK4(y,t(2),t(3),2,v);
F3 = v(t(3), y);
y = odeRK4(y,t(3),t(4),2,v);
F4 = v(t(4), y);

for i=4:Nt
    y=y+dt*(55/24*F4 -59/24*F3+37/24*F2-3/8*F1);
    F1=F2;
    F2=F3;
    F3=F4;
    F4=v(t(i+1),y);
end
end