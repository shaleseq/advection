function y = odeRK2(y0,t0,tf,n, v)
t=linspace(t0,tf,n);
Nt=length(t)-1;
dt=t(2)-t(1);
h=dt;
y=y0;
for i=1:Nt
    k1=v(t(i),y)*h;
    k2=v(t(i)+2/3*dt, y+2/3*k1)*h;
    y=y+1/4*k1+3/4*k2;
end

end
