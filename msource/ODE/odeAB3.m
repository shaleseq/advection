function y = odeAB3(y0,t0,tf,n, v)
t=linspace(t0,tf,n);
Nt=length(t)-1;
dt=t(2)-t(1);
y=y0;
F1 = v(t(1),y);
y = odeRK3(y,t(1),t(2),2,v);
F2 = v(t(2), y);
y = odeRK3(y,t(2),t(3),2,v);
F3 = v(t(3), y);
for i=3:Nt
    y=y+dt*(23/12*F3 -4/3*F2+5/12*F1);
    F1=F2;
    F2=F3;
    F3=v(t(i+1),y);
end
end