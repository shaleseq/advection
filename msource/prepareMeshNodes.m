function MESH = prepareMeshNodes( MESH, reorder)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

if nargin < 2
    reorder = 1;
end

nel=length(MESH.ELEMS);
nv = length(MESH.NODES);

if reorder
    reorder = 'metis';
    opts.reordering_type = reorder;
    MESH = mesh_reorder(MESH,opts);
end


center = zeros(2,nel);
center(1,:) = mean(reshape(MESH.NODES(1,MESH.ELEMS(1:3,:)),3,nel));
center(2,:) = mean(reshape(MESH.NODES(2,MESH.ELEMS(1:3,:)),3,nel));

X = MESH.NODES(1, :);
Y = MESH.NODES(2, :);
idx = MESH.ELEMS;

segments = zeros(2, 3, nel);
segments(:,1,:) = [(X(idx(2,:))+X(idx(1,:)))/2; (Y(idx(2,:))+Y(idx(1,:)))/2];
segments(:,2,:) = [(X(idx(3,:))+X(idx(2,:)))/2; (Y(idx(3,:))+Y(idx(2,:)))/2];
segments(:,3,:) = [(X(idx(1,:))+X(idx(3,:)))/2; (Y(idx(1,:))+Y(idx(3,:)))/2];



%% REORDER 2
EL = MESH.ELEMS;
nw = length(EL(1,:));
nh = length(EL(:,1));
EL2ED = zeros(1, nw*nh);
b=[2:nh 1];

ED = MESH.EDGES;
n=length(MESH.NODES(1,:));
nl = length(ED(1,:));
tED = uint64([ED ED([2,1],:)]);

tEL1 = reshape(EL, 1, nw*nh);
tEL2 = reshape(EL(b,:),1, nw*nh);
tEL = uint64([tEL1;tEL2]);

tED = tED(1,:)+(tED(2,:)-1)*n;
tEL = tEL(1,:)+(tEL(2,:)-1)*n;

[~,B,C]=intersect(tED,tEL);

EL2ED(C)=B;
EL2ED(EL2ED>nl)=-EL2ED(EL2ED>nl)+nl;
EL2ED=reshape(EL2ED, nh, nw);
EL2ED2 = EL2ED;
id = EL2ED < 0;
EL2ED2(id) = abs(EL2ED2(id))+nl;

ED2EL = zeros(1, nl*2);
ED2EL(EL2ED2) = reshape(repmat([1:nw], nh, 1),1,nh*nw);


%% NEXT STEP
centers = repmat(reshape(center, 2, 1, []), [1, 3, 1]);
vol = zeros(1, nv);
MESH.SEGMENTS = (segments + centers) ./ 2;

lx = zeros(1, 3, nel);
ly = zeros(1, 3, nel);

lx = centers(1,:,:) - segments(1,:,:);
ly = centers(2,:,:) - segments(2,:,:);


MESH.LENGTH = squeeze(sqrt(lx.^2+ly.^2));
MESH.NORMALS = getNorm(lx, ly);

subs = repmat(MESH.ELEMS, 1, 2);
v2 = MESH.NODES(:, repmat(MESH.ELEMS, 2, 1));
c2 = reshape(repmat(reshape(center, 2, 1, []), [1, 6, 1]),2,[]);
sn = repmat(segments, [1, 2, 1]);
sn(:, 2, :) = sn(:, 1, :);
sn(:, 4, :) = sn(:, 3, :);
sn(:, 6, :) = sn(:, 5, :);
sn = reshape(sn, 2, []);
val = polyarea([v2(1, :); c2(1,:); sn(1,:)],[v2(2,:); c2(2,:); sn(2,:)]);


MESH.AREA = accumarray(subs(:), val);
test = mean(sqrt(MESH.NORMALS(1,:).^2+MESH.NORMALS(2,:).^2));

display('Start Tests:');
display(['MESH AREA = ' num2str(sum(MESH.AREA))]);
display(['LENGTH MESH NORMALS = ' num2str(test)]);

MESH.Nx = squeeze(MESH.NORMALS(1,:,:)).*MESH.LENGTH;
MESH.Ny = squeeze(MESH.NORMALS(2,:,:)).*MESH.LENGTH;

MESH.ED2EL=uint32(ED2EL);
MESH.NEIGHBOURS = MESH.ELEMS([2 3 1]',:);
return;
%% TEST PLOT
%1
figure(4)
clf;
hold on;
vnodes = [segments(1:2,:) centers(1:2,:)];
h=trisurf(MESH.ELEMS', MESH.NODES(1,:), MESH.NODES(2, :), zeros(1, length(MESH.NODES)));
set(h,'linewidth',2);
% patch(reshape(vnodes(1, vedges), 2, []),reshape(vnodes(2, vedges), 2, []), 'g');
plot(vnodes(1,:), vnodes(2, :), 'r*');
plot(MESH.NODES(1, :), MESH.NODES(2, :), 'b+');
patch([segments(1,:);centers(1,:)], [segments(2,:);centers(2,:)], 'r');
quiver(MESH.SEGMENTS(1, :), MESH.SEGMENTS(2, :), MESH.NORMALS(1, :), MESH.NORMALS(2,:));
axis equal, axis tight;
%2
figure(5)
clf;
hold on;
%trisurf(MESH.ELEMS', MESH.NODES(1,:), MESH.NODES(2, :), zeros(1, length(MESH.NODES)));
% patch(reshape(vnodes(1, vedges), 2, []),reshape(vnodes(2, vedges), 2, []), 'g');
% plot(vnodes(1,:), vnodes(2, :), 'r*');
% plot(MESH.NODES(1, :), MESH.NODES(2, :), 'b+');
% quiver(MESH.SEGMENTS(1,:), MESH.SEGMENTS(2,:), MESH.NORMALS(1,:), MESH.NORMALS(2,:));
h=trisurf(MESH.ELEMS(:,1)', MESH.NODES(1,:), MESH.NODES(2,:), zeros(1, length(MESH.NODES)));
set(h,'linewidth',2);
plot(segments(1,1:3), segments(2, 1:3), 'r*');
plot(centers(1,1:3), centers(2, 1:3), 'r*');
plot(MESH.NODES(1, MESH.ELEMS(:,1)), MESH.NODES(2, MESH.ELEMS(:,1)), 'b+');
quiver(MESH.SEGMENTS(1, 1:3), MESH.SEGMENTS(2, 1:3), MESH.NORMALS(1, 1:3), MESH.NORMALS(2, 1:3));
patch([segments(1,1:3);centers(1,1:3)], [segments(2,1:3);centers(2,1:3)], 'r');
axis equal;
a=1;
end

