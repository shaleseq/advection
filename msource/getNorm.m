function n = getNorm( x1, y1 )
    l = sqrt(x1.^2+y1.^2);
    n=[y1./l;-x1./l];
end

