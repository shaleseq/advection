function [ T ] = advectionMElems( MESH,T, dt, vx,vy, method)
if nargin < 6
    method =  'upwind';
end
switch method
    case 'upwind'
        T = advectionMElemsUpwind(MESH,T,dt,vx,vy);
    case 'laxwen'
       T = advectionMElemsLaxWen(MESH,T,dt,vx,vy);
    otherwise
           display('avaible methods upwind, laxwen');
           T=[];
end
end