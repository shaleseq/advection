clear

addpath('/home/repos/milamin/MILAMIN_v2/trunk/src/')
milamin_init(2,4)
addpath(genpath('/home/repos/milamin/MILAMIN_v2/branches/mutils-0.5/'))


nmesh = [4];

for im=1:length(nmesh)
%% read mesh
clear MSH
load(['../poisson_flow/mesh_npts_', num2str(nmesh(im))])

MSH.ELEMS = ELEMS;
MSH.NODES = NODES;
MSH.SEGMENTS = SEGMENTS;
MSH.elem_markers = elem_markers;
MSH.node_markers = node_markers;
MSH.segment_markers = segment_markers;  
MSH = mesh_info(MSH);
MSH = mesh_convert(MSH, 'tri7');

%% computational box
xmin = min(MSH.NODES(1,:));
xmax = max(MSH.NODES(1,:));
ymin = min(MSH.NODES(2,:));
ymax = max(MSH.NODES(2,:));

%% periodicity 
eps = 1e-6;
% x direction
L = find(abs(MSH.NODES(1,:)-xmin)<eps);
R = find(abs(MSH.NODES(1,:)-xmax)<eps);
[~,ind] = sort(MSH.NODES(2,L));
indx_L = L(ind); %indx_L(1) = []; indx_L(end)=[];
[~,ind] = sort(MSH.NODES(2,R));
indx_R = R(ind); %indx_R(1) = []; indx_R(end)=[];
MSH.periodic_nodes.x = uint32([indx_L; indx_R]); 


% y direction
B = find(abs(MSH.NODES(2,:)-ymin)<eps);
T = find(abs(MSH.NODES(2,:)-ymax)<eps);
[~,ind] = sort(MSH.NODES(1,B));
indx_B = B(ind); %indx_B(1) = []; indx_T(end)=[];
[~,ind] = sort(MSH.NODES(1,T));
indx_T = T(ind); %indx_B(1) = []; indx_T(end)=[];
MSH.periodic_nodes.y = uint32([indx_B; indx_T]); 
MSH = mesh_set_periodicity(MSH, 'xy');

%opts.periodic_show = 1;
%mesh_show(MSH, ones(1,MSH.info.nel)',opts)

% reorder
reorder = 'metis';
opts.reordering_type = reorder;
MSH = mesh_reorder(MSH,opts);

% MSH info
[nnodel,nel]=size(MSH.ELEMS);
[ndim,nnod]=size(MSH.NODES);

%% Stokes solver

% boundary conditions
BC = bc_init(MSH,2);
% inclusion rims - no slip, V=0
BC = bc_update('dirichlet',BC, find(MSH.node_markers==5), 1, 0);
BC = bc_update('dirichlet',BC, find(MSH.node_markers==5), 2, 0);

el_Mu = ones(MSH.info.nel,1);

H = src_init(MSH,1);
ind_all = 1:MSH.info.nel;
sf = @(x,y,t)1;
H = src_update(H,ind_all,1,sf);
H = src_eval(H, MSH);
rhs = zeros(MSH.info.ndim*MSH.info.nnod,1);
rhs(1:MSH.info.ndim:end) = H.val;
[Vel, p] = flow2d(MSH,BC,el_Mu,rhs); 

%% Post-processing
Vm = sqrt(Vel(1,:).^2+Vel(2,:).^2);
mesh_show(MSH, Vm')
caxis([0 5e-3])

%print(gcf,'-dpng','-r600', ['Vmag_', num2str(nmesh(im), '%.3d')] )

end