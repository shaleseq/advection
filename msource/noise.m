function x = noise(DIM,BETA)

u = [(0:floor(DIM/2)) -(ceil(DIM/2)-1:-1:1)]'/DIM;
u = repmat(u,1,DIM);
v = u';


k = (u.^2 + v.^2).^(BETA/2);



phase_shift = zeros(DIM);    


if(mod(DIM,2)==0)
    phi = [rand((DIM-2)/2,1); 0];
    
    tmp = (cos(2*pi*phi)+i*sin(2*pi*phi));
    phase_shift(2:end,1) = [tmp; conj(flipud(tmp(1:end-1)))];

    phi = [rand(1,(DIM-2)/2) 0];

    tmp = (cos(2*pi*phi)+i*sin(2*pi*phi));
    phase_shift(1,2:end) = [tmp conj(fliplr(tmp(1:end-1)))];

    
    alpha = rand((DIM-2)/2+1);
    alpha(:,end) = 0; 
    beta  = rand((DIM-2)/2+1);
    beta(end,:) = 0;
    
    ph1 = (cos(2*pi*alpha)+i*sin(2*pi*alpha));
    ph2 = (cos(2*pi*beta)+i*sin(2*pi*beta));

    tmp = [ph1.*ph2                  fliplr(conj((ph1(:,1:end-1))).*ph2(:,1:end-1));...
    flipud(ph1(1:end-1,:).*conj(ph2(1:end-1,:)))   fliplr(flipud(conj(ph1(1:end-1,1:end-1).*ph2(1:end-1,1:end-1))))];

    phase_shift(2:end,2:end) = tmp;
    

else
    phi = rand((DIM-1)/2,1);
    tmp = (cos(2*pi*phi)+i*sin(2*pi*phi));    
    phase_shift(2:end,1) = [tmp; conj(flipud(tmp))];

    phi = rand(1, (DIM-1)/2 );
    
    tmp = (cos(2*pi*phi)+i*sin(2*pi*phi));
    phase_shift(1,2:end) = [tmp conj(fliplr(tmp))];

    alpha = rand((DIM-1)/2);
    beta = rand((DIM-1)/2);

    ph1 = (cos(2*pi*alpha)+i*sin(2*pi*alpha));
    ph2 = (cos(2*pi*beta)+i*sin(2*pi*beta));

    tmp = [ph1.*ph2                  fliplr(conj((ph1)).*ph2);...
    flipud(ph1.*conj(ph2))   fliplr(flipud(conj(ph1.*ph2)))];

    phase_shift(2:end,2:end) = tmp;
end

k(k==Inf) = 0;

%energy scaling
%k^2 ~ f^beta

x = ifft2(k.^0.5.*phase_shift);



%x = real(x);
