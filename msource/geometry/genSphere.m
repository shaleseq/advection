function Plc = genSphere( rlevel)
% functions return sphere in plc format
%rlevel is number of recursion for generating sphere
%for examples see testGeom.m
t = (1.0 + sqrt(5.0)) / 2.0;
points = zeros(3, 12);
points(:,1) = [-1;  t;  0];
points(:,2) = [ 1;  t;  0];
points(:,3) = [-1; -t;  0];
points(:,4) = [ 1; -t;  0];

points(:,5) = [ 0; -1;  t];
points(:,6) = [ 0;  1;  t];
points(:,7) = [ 0; -1; -t];
points(:,8) = [ 0;  1; -t];

points(:,9) = [ t;  0; -1];
points(:,10) = [ t;  0;  1];
points(:,11) = [-t;  0; -1];
points(:,12) = [-t;  0;  1];

len = sqrt(dot(points, points));
points = points./repmat(len, 3, 1);
faces = zeros(3, 20);


faces(:,1) = [0; 11; 5];
faces(:,2) = [0; 5; 1];
faces(:,3) = [0; 1; 7];
faces(:,4) = [0; 7; 10];
faces(:,5) = [0; 10; 11];

faces(:,6) = [1; 5; 9];
faces(:,7) = [5; 11; 4];
faces(:,8) = [11; 10; 2];
faces(:,9) = [10; 7; 6];
faces(:,10) = [7; 1; 8];

faces(:,11) = [3; 9; 4];
faces(:,12) = [3; 4; 2];
faces(:,13) = [3; 2; 6];
faces(:,14) = [3; 6; 8];
faces(:,15) = [3; 8; 9];

faces(:,16) = [4; 9; 5];
faces(:,17) = [2; 4; 11];
faces(:,18) = [6; 2; 10];
faces(:,19) = [8; 6; 7];
faces(:,20) = [9; 8; 1];

faces = faces+1;
for i=1:rlevel
    
    faces2 = zeros(3, length(faces)*4);
    for j=1:length(faces)
        a = getMiddlePoint(faces(1,j), faces(2,j));
        b = getMiddlePoint(faces(2,j), faces(3,j));
        c = getMiddlePoint(faces(3,j), faces(1,j));
        
        faces2(:,(j-1)*4+1) = [faces(1,j); a; c];
        faces2(:,(j-1)*4+2) = [faces(2,j); b; a];
        faces2(:,(j-1)*4+3) = [faces(3,j); c; b];
        faces2(:,j*4) = [a; b; c];
    end
    faces = faces2;
end
Plc.pointlist = points;
Plc.facetlist = int32(faces);


%% Functions
    function id = addVertex(point)
        len = norm(point);
        point = point./len;
        points = [points point];
        id = length(points);
    end
    function id = getMiddlePoint(id1, id2)
        
        p1 = points(:,id1);
        p2 = points(:,id2);
        middle = (p1+p2)./2;
        id = addVertex(middle);
    end


end



