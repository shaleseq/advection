function Plc = genCube(x0, xf, y0, yf, N, hpf, hnf)
%function return cube in plc format
%x0,xf-x domain
%y0,yf-y domain
%N-number of points for discretization
%hpf-function for top face
%hnf-function for bottom face
%for examples see testGeom.m
x = linspace(x0, xf, N);
y = linspace(y0, yf, N);
[X,Y] = meshgrid(x,y);

Zp = hpf(X,Y);
Zn = hnf(X,Y);

pointsp = [X(:)';Y(:)';Zp(:)'];
pointsn = [X(:)';Y(:)';Zn(:)'];
points = [pointsp pointsn];
M=length(pointsp);

tri = zeros(3, 2*(N-1)*(N-1));
for j=1:N-1
    tri(:,2*(j-1)*(N-1)+1) = [1+(j-1)*N;2+(j-1)*N;N+1+(j-1)*N];
    for i=2:N-1
        tri(:,2*((i-1)+(j-1)*(N-1))) = [i+(j-1)*N; i+1+(j-1)*N; i+j*N];
        tri(:,2*((i-1)+(j-1)*(N-1))+1) = [i+(j-1)*N; i+j*N; (i-1)+j*N];
    end
    tri(:,2*j*(N-1)) = [j*N;(j+1)*N;(N-1)+j*N];
end


trib = zeros(3, (N-1)*8);

for j=1:N-1
    trib(:,(j-1)*2+1) = [M+j;j;M+j+1];
    trib(:,j*2) = [M+j+1;j;j+1];
    
    trib(:,(j-1)*2+1+2*(N-1)) = [(N-1)*N+j;M+(N-1)*N+j;M+(N-1)*N+j+1];
    trib(:,j*2+2*(N-1)) = [(N-1)*N+j;M+(N-1)*N+j+1;(N-1)*N+j+1];
    
    trib(:,(j-1)*2+1+4*(N-1)) = [(j-1)*(N)+1; M+(j-1)*(N)+1;  M+(j)*(N)+1];
    trib(:,j*2+4*(N-1)) = [ (j-1)*(N)+1; M+(j)*(N)+1; (j)*(N)+1];
    
    trib(:,(j-1)*2+1+6*(N-1)) = [N*j+M; N*j;  N*(j+1)+M];
    trib(:,j*2+6*(N-1)) = [N*(j+1)+M; N*j;  N*(j+1)];
end

tri = [[tri(2,:); tri(1,:); tri(3,:)] tri+M trib];
Plc.pointlist = points;
Plc.facetlist = int32(tri);

end

