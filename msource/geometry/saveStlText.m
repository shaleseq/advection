function  saveStlText(tri, points, file)
%% tri, points, filename
f = fopen(file, 'w');
N = length(tri);
fprintf(f,'solid name\n');
for i=1:N
    m = points(:, tri(:,i));
    normal = cross(m(:,2)-m(:,1), m(:,3)-m(:,1));
    normal = normal ./ sqrt(dot(normal, normal));
    fprintf(f, 'facet normal %f %f %f\n', normal);
    fprintf(f, '\touter loop\n');
    fprintf(f, '\t\tvertex %f %f %f\n', m(:,1));
    fprintf(f, '\t\tvertex %f %f %f\n', m(:,2));
    fprintf(f, '\t\tvertex %f %f %f\n', m(:,3));
    fprintf(f, '\tendloop\n');
    fprintf(f, 'endfacet\n');
end
fprintf(f,'endsolid name\n');
fclose(f);
end

