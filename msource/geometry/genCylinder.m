function Plc = genCylinder(r,z,Nr, Nz)
%function return cylinder in Plc format
%r-radius of cylinder
%z-hight of cylinder
%Nr-number of points for phi discretization
%Nz-number of points for z discretization
%for examples see testGeom.m
Plc = [];
points = zeros(3, Nr*Nz);
tri = zeros(3, Nr*(Nz-1)*2);
zz=linspace(0, z, Nz);
%dp = 2*pi/Nr;
for j = 1:Nz
    for i=1:Nr
        phi = (i-1)*2*pi/Nr;
        points(:,(j-1)*Nr+i)=[r*cos(phi);r*sin(phi);zz(j)];
        
    end
end

for j=1:Nz-1
    for i=1:Nr-1
        tri(:, 2*((j-1)*Nr+(i-1))+1) = [i+(j-1)*Nr; i+(j-1)*Nr+1; i+j*Nr+1];
        tri(:, 2*((j-1)*Nr+i)) = [i+j*Nr+1; i+j*Nr; i+(j-1)*Nr];
    end
    tri(:, 2*((j-1)*Nr+(Nr-1))+1) = [Nr+(j-1)*Nr; (j-1)*Nr+1; j*Nr+1];
    tri(:, 2*((j-1)*Nr+Nr)) = [j*Nr+1; Nr+j*Nr; Nr+(j-1)*Nr];
end

bp = [0 0;0 0;0 10];
s1 = length(points)+1;
s2 = length(points)+2;
btri =zeros(3, Nr*2);

points = [points bp];

for i=1:Nr-1
    btri(:,2*(i-1)+1)=[i;s1;i+1];
    btri(:,2*i)=[(Nz-1)*Nr+i;s2;(Nz-1)*Nr+i+1];
end
btri(:,Nr*2-1) = [Nr;s1;1];
btri(:,Nr*2) = [Nr*Nz;s2;(Nz-1)*Nr+1];

tri = [tri btri];
Plc.pointlist = points;
Plc.facetlist = int32(tri);
end

