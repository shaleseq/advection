function T = toNodes( MESH, Tc )
    alpha = [MESH.NODES(1, MESH.ELEMS) - reshape(repmat(MESH.CENTER(1, :), 3, 1), 1, [])... 
        ;MESH.NODES(2, MESH.ELEMS) - reshape(repmat(MESH.CENTER(2,:), 3, 1), 1, [])];
    alpha = sqrt(dot(alpha, alpha));
    T = accumarray(MESH.ELEMS(:), alpha.*reshape(repmat(Tc,3,1), 1, []));
    val = accumarray(MESH.ELEMS(:), alpha);
    T = T./val;
end

