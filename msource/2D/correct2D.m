function [Ac,Bc] = correct2D( A,B, phi, rx,ry, u)
%%Params
N=length(A);
f1=[2:N 1];
f2=[3:N 1 2];
b1=[N 1:N-1];
b2=[N-1 N 1:N-2];

%% Zalesak
%antidiffusive to zero
A(A.*(phi(f1,:)-phi) < 0 | A.*(phi(f2,:)-phi(f1,:)) < 0 | A.*(phi-phi(b1,:)) < 0)=0;
B(B.*(phi(:,f1)-phi) < 0 | B.*(phi(:,f2)-phi(:,f1)) < 0 | B.*(phi-phi(:,b1)) < 0)=0;

%%max

mat1= cat(3,phi(b1,:), phi, phi(f1,:), u(f1,:), u, u(b1,:)...
    ,phi(:,b1), phi(:,f1), u(:,f1), u(:,b1));
phiMax=max(mat1,[],3);
phiMin=min(mat1,[],3);

%% X
% right side +
PpA=max(zeros(N),A(b1,:))-min(zeros(N),A);
QpA=(phiMax-phi)*rx;
RpA=arrayfun(@gR, QpA, PpA);

% left side -
PmA=max(zeros(N),A)-min(zeros(N),A(b1,:));
QmA=(phi-phiMin)*rx;
RmA=arrayfun(@gR, QmA, PmA);
% result
CA=(A>=0).*min(RpA(f1,:),RmA)+(A<0).*min(RpA,RmA(f1,:));
Ac=CA.*A;

%% Y
%right side +
PpB=max(zeros(N),B(:,b1))-min(zeros(N),B);
QpB=(phiMax-phi)*ry;
RpB=arrayfun(@gR, QpB, PpB);

%left side -
PmB=max(zeros(N),B)-min(zeros(N),B(:,b1));
QmB=(phi-phiMin)*ry;
RmB=arrayfun(@gR, QmB, PmB);
%result
CB=(B>=0).*min(RpB(:,f1),RmB)+(B<0).*min(RpB,RmB(:,f1));
Bc=CB.*B;


%% FUNC
    function y=gR(q,p)
        if(p >0)
            y=min(1, q/p);
        else
            y=0;
        end
        
    end
end

