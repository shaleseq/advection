function [ u ] = fluxCorrectorAdvection2D( x0, y0,z0, t, vx,vy, method)
%function return numeric solution of advection equation
%args x0-points,y0-initial values, t-time, v-velocity, method default 'laxwen'.

Nt = length(t);
Mx = length(x0);
My = length(y0);
u = zeros(Mx, My, Nt);
u(:,:, 1) = z0;
N = Nt - 1;
dt=t(2)-t(1);
dx=x0(2)-x0(1);
dy=y0(2)-y0(1);
rx=vx*dt/dx/2;
ry=vy*dt/dy/2;
Rx=dt/dx;
Ry=dt/dy;
f=[2:Mx 1];
b=[Mx 1:Mx-1];
b2=[Mx-1 Mx 1:Mx-2];
switch method
    case 'upwindC'
        for i=1:N
            % Calculate upwind
            %f1=vx*u(:,:,i);
            %g1=vy*u(:,:,i);
            % Calculate LaxWen
            %f2=0.5*vx*(u(:,:,i)+u(f,:,i))-vx*rx*(u(f,:,i)-u(:,:,i));
            %g2=0.5*vy*(u(:,:,i)+u(:,f,i))-vy*ry*(u(:,f,i)-u(:,:,i));
            
            f1=vx./2.*(u(:,f,i)+u(:,:,i))-abs(vx./2).*(u(:,f,i)-u(:,:,i));
            g1=vy./2.*(u(f,:,i)+u(:,:,i))-abs(vy./2).*(u(f,:,i)-u(:,:,i));
            
            f2 = 0.5.*vx.*(u(:,:,i) + u(:, f, i)) - Rx./2.*vx.^2.*(u(:, f, i)-u(:,:,i))-...
               Ry./8.*vx.*vy.*(u(f,f,i)+u(f,:,i)-u(b,f,i)-u(b,:,i));
           
            g2 = 0.5.*vy.*(u(:,:,i) + u(f, :, i)) - Ry./2.*vy.^2.*(u(f, :, i)-u(:,:,i))-...
               Rx./8.*vx.*vy.*(u(f,f,i)+u(:,f,i)-u(f,b,i)-u(:,b,i));
            
            A=f2-f1;
            B=g2-g1;
            % Calculate value
            phi=u(:,:,i) -Rx*(f1(:,:)-f1(b,:))-Ry*(g1(:,:)-g1(:,b));%1R solve
            % Aplay correction
            
            [Ac,Bc]=correct2D(A,B, phi,1/Rx,1/Ry,u(:,:, i));%correct solution
            
            u(:, :,i+1) = phi-Rx*(Ac(:,:)-Ac(b,:))-Ry*(Bc(:,:)-Bc(:,b));%apply correction
        end
        
    case 'laxC'
        for i=1:N
            % Calculate Lax
            f1=0.5*vx*(u(f,:,i)+u(:,:,i))- (dx*vx/(2*dt))*(u(f,:,i)-u(:,:,i));
            g1=0.5*vy*(u(:,f,i)+u(:,:,i))- (dy*vy/(2*dt))*(u(:,f,i)-u(:,:,i));
            
             % Calculate LaxWen
            %f2=0.5*vx*(u(:,:,i)+u(f,:,i))-vx*rx*(u(f,:,i)-u(:,:,i));
            %g2=0.5*vy*(u(:,:,i)+u(:,f,i))-vy*ry*(u(:,f,i)-u(:,:,i));
            A=f2-f1;
            B=g2-g1;
            % Calculate value
            phi=u(:,:,i)-Rx*(f2(:,:)-f2(b,:))-Ry*(g2(:,:)-g2(:,b));%1R solve
            % Aplay correction
            
            [Ac,Bc]=correct2D(A,B, phi,1/Rx,1/Ry,u(:,:, i));%correct solution
            
           u(:, :,i+1) = phi;%-Rx*(Ac(:,:)-Ac(b,:))-Ry*(Bc(:,:)-Bc(:,b));%apply correction
        end
    otherwise
        display('avaible methods: ftcs, lax, middle, laxwen, upwind, upwind2R');
        u = [];
        return;
end

end



