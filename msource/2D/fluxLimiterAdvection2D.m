function [ u ] = fluxLimiterAdvection2D( x0, y0,z0, t, vx,vy, method, limiter)
%function return numeric solution of advection equation
%args x0,y0-points,z0-initial values, t-time, v-velocity, method default 'laxwen'.

Nt = length(t);
Mx = length(x0);
My = length(y0);
u = zeros(Mx, My, Nt);
u(:,:, 1) = z0;
N = Nt - 1;
dt=t(2)-t(1);
dx=x0(2)-x0(1);
dy=y0(2)-y0(1);
%[X,Y]=meshgrid(x0,y0);
Rx=dt/dx;
Ry=dt/dy;
rx=abs(vx.*dt./dx);
ry=abs(vy.*dt./dy);

f=[2:Mx 1];
b=[Mx 1:Mx-1];

    function y=bc(i)
        y=i+Mx;
        y=mod(y,Mx);
        if y==0
            y=Mx;
        end
    end
            
switch method
    case 'upwind'
        for i=1:N
          f1=vx./2.*(u(:,f,i)+u(:,:,i))-abs(vx./2).*(u(:,f,i)-u(:,:,i));
          g1=vy./2.*(u(f,:,i)+u(:,:,i))-abs(vy./2).*(u(f,:,i)-u(:,:,i));
          u(:,:,i+1) = u(:,:,i) -Rx*(f1-f1(:,b)) -Ry*(g1-g1(b,:));
        end
    case 'laxwen'
        for i=1:N
          %Limiters
          gx=-sign(vx);
          gy=-sign(vy);
          rpx=zeros(My,Mx);
          rpy=zeros(My,Mx);
          
          for j=1:Mx
              for k=1:My
                  rpx(k,j)=(u(k,bc(j+1+gx(k,j)),i)-u(k, bc(j+gx(k,j)),i))./(u(k,bc(1+j),i)-u(k,j,i));
                  rpy(k,j)=(u(bc(k+1+gy(k,j)),j,i)-u(bc(k+gy(k,j)),j,i))./(u(bc(1+k),j,i)-u(k,j,i));
              end
          end
          
          rpx(isnan(rpx)|isinf(rpx))=0;
          cpx=fluxLimiter2D(rpx, limiter);
          
          rpy(isnan(rpy)|isinf(rpy))=0;
          cpy=fluxLimiter2D(rpy, limiter);
          
          f1=vx./2.*(u(:,f,i)+u(:,:,i))-abs(vx./2).*(u(:,f,i)-u(:,:,i));
          g1=vy./2.*(u(f,:,i)+u(:,:,i))-abs(vy./2).*(u(f,:,i)-u(:,:,i));
          
          f2 = 0.5.*vx.*(u(:,:,i) + u(:, f, i)) - Rx./2.*vx.^2.*(u(:, f, i)-u(:,:,i))-...
               Ry./8.*vx.*vy.*(u(f,f,i)+u(f,:,i)-u(b,f,i)-u(b,:,i));
           
           g2 = 0.5.*vy.*(u(:,:,i) + u(f, :, i)) - Ry./2.*vy.^2.*(u(f, :, i)-u(:,:,i))-...
               Rx./8.*vx.*vy.*(u(f,f,i)+u(:,f,i)-u(f,b,i)-u(:,b,i));
          
          
          f2=f1+(f2-f1).*cpx;
          g2=g1+(g2-g1).*cpy;
          
          %result
          u(:,:,i+1) = u(:,:,i) -Rx*(f2-f2(:,b)) -Ry*(g2-g2(b,:));
        end
    otherwise
        display('avaible methods: ftcs, lax, middle, laxwen, upwind, upwind2R');
        u = [];
        return;
end

end