function y = fluxLimiter2D(x, method)
switch method
    case 'upwind'
        y=0*x;
    case 'laxwen'
        y=ones(size(x));
    case 'minmod'
        y=max(zeros(size(x)),min(ones(size(x)),x));
    case 'superbee'
        y=max(max(zeros(size(x)),min(ones(size(x)),2*x)),min(2*ones(size(x)),x));
    otherwise
        y=0*x;
        display('choose limiter');
end
end

