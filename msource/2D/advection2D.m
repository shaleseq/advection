function [ u ] = advection2D( x0, y0,z0, t, vx,vy, method )

if nargin < 7
    method =  'laxwen';
end
Nt = length(t);
Mx = length(x0);
My = length(y0);
u = zeros(Mx, My, Nt);
u(:,:, 1) = z0;
dx = x0(2)-x0(1);
dy = y0(2)-y0(1);
dt = t(2) - t(1);
rx=vx*dt./dx;
ry=vy*dt./dy;
N = Nt - 1;
f = [2:Mx 1];
b = [Mx 1:Mx-1];
switch method
    case 'upwind'
        for i = 1:N
            u(:,:, i+1) = u(:,:, i) -ry.*(u(:,:,i) - u([end 1:end-1],:, i)) -rx.*(u(:,:,i) - u(:,[end 1:end-1],i));
        end
    case 'middle'
        %upwind for second point
        u(:,:, 2) = u(:,:, 1) -ry.*(u(:,:,1) - u(b,:, 1)) -rx.*(u(:,:,1) - u(:,b,1));
        for i = 2 : N
            u(:, :,i+1) = u(:,:, i-1) - ry.*(u([2:end 1],:, i) - u([end 1:end-1],:, i))...
                - rx.*(u(:,[2:end 1], i) - u(:,[end 1:end-1], i));
        end
    case 'lax'
        for i = 1:N
             u(:,:, i+1) = 0.25*(u(f,:, i) + u(b,:, i)+ u(:,f, i) + u(:,b, i) )...
                - ry./2.*(u(f,:, i) - u(b,:, i)) - rx./2.*(u(:,f, i) - u(:,b, i));

        end
    case 'laxwen'
        %stability rx^(2/3)+ry^(2/3)<1
        for i=1:N
            u(:,:, i+1) = u(:,:, i) -ry .* (0.5*(u([2:end 1],:, i)-u([end 1:end-1],:, i))+...
                ry./2 .*(2*u(:,:,i) - u([end 1:end-1],:, i)-u([2:end 1],:, i))) ...
                -rx .* (0.5*(u(:,[2:end 1], i)-u(:,[end 1:end-1], i))+...
                rx./2 .*(2*u(:,:,i) - u(:,[end 1:end-1], i)-u(:,[2:end 1], i)))+...
                0.25*rx.*ry.*(u(f,f, i) + u(b,b,i) - u(f,b,i) - u(b,f,i));   
        end
        
    otherwise
        display('avaible methods:lax, middle, laxwen, upwind');
        u = [];
        return;
end
end

