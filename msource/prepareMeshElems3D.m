function MESH = prepareMeshElems3D( MESH)
WS=[];
MESH.ELEMS=uint32(MESH.ELEMS);
points = [0;0;0];
nel=length(MESH.ELEMS);
[TR,WS] = tsearch2(MESH.NODES, MESH.ELEMS,points, WS, [], []);
MESH.CENTER=WS.ELEMS_CENTERS;
MESH.NEIGHBOURS=WS.NEIGHBORS;
idx = MESH.ELEMS;
%% NORMALS && 
MESH.NORMALS = zeros(3, 4, nel);
MESH.FCENTER = zeros(3,4,nel);
faces = [2 3 4; 1 3 4; 1 2 4; 1 2 3]';

FaceNodeX = reshape(MESH.NODES(1, idx), 4, []);
FaceNodeY = reshape(MESH.NODES(2, idx), 4, []); 
FaceNodeZ = reshape(MESH.NODES(3, idx), 4, []);

FaceNodeX = reshape(FaceNodeX(faces(:), :), 3, []);
FaceNodeY = reshape(FaceNodeY(faces(:), :), 3, []);
FaceNodeZ = reshape(FaceNodeZ(faces(:), :), 3, []);



MESH.FCENTER(1, :,:) = reshape(mean(FaceNodeX), 1, 4, []);
MESH.FCENTER(2, :,:) = reshape(mean(FaceNodeY), 1, 4, []);
MESH.FCENTER(3, :,:) = reshape(mean(FaceNodeZ), 1, 4, []);

X = MESH.NODES(1, :);
Y = MESH.NODES(2, :);
Z = MESH.NODES(3,:); 

lx1 = X(idx(2,:))-X(idx(1,:));
ly1 = Y(idx(2,:))-Y(idx(1,:));
lz1 = Z(idx(2,:))-Z(idx(1,:));


lx2 = X(idx(3,:))-X(idx(1,:));
ly2 = Y(idx(3,:))-Y(idx(1,:));
lz2 = Z(idx(3,:))-Z(idx(1,:));

lx3 = X(idx(4,:))-X(idx(1,:));
ly3 = Y(idx(4,:))-Y(idx(1,:));
lz3 = Z(idx(4,:))-Z(idx(1,:));

lx4 = X(idx(3,:))-X(idx(2,:));
ly4 = Y(idx(3,:))-Y(idx(2,:));
lz4 = Z(idx(3,:))-Z(idx(2,:));

lx5 = X(idx(4,:))-X(idx(2,:));
ly5 = Y(idx(4,:))-Y(idx(2,:));
lz5 = Z(idx(4,:))-Z(idx(2,:));

r1 = [lx1;ly1;lz1];
r2 = [lx2; ly2; lz2];
r3 = [lx3; ly3; lz3];
r4 = [lx4; ly4; lz4];
r5 = [lx5; ly5; lz5];



MESH.VOL = abs(dot(cross(r1, r2), r3))./6; 


MESH.NORMALS(:,1,:) = cross(r4, r5);
MESH.NORMALS(:,2,:) = cross(r3, r2);
MESH.NORMALS(:,3,:) = cross(r1, r3);
MESH.NORMALS(:,4,:) = cross(r2, r1);

MESH.AREA = zeros(4,nel);
MESH.AREA(1,:) = sqrt(dot(squeeze(MESH.NORMALS(:,1,:)), squeeze(MESH.NORMALS(:,1,:))));
MESH.AREA(2,:) = sqrt(dot(squeeze(MESH.NORMALS(:,2,:)), squeeze(MESH.NORMALS(:,2,:))));
MESH.AREA(3,:) = sqrt(dot(squeeze(MESH.NORMALS(:,3,:)), squeeze(MESH.NORMALS(:,3,:))));
MESH.AREA(4,:) = sqrt(dot(squeeze(MESH.NORMALS(:,4,:)), squeeze(MESH.NORMALS(:,4,:))));


MESH.Nx = squeeze(MESH.NORMALS(1, :, :));
MESH.Ny = squeeze(MESH.NORMALS(2, :, :));
MESH.Nz = squeeze(MESH.NORMALS(3, :, :));

MESH.NORMALS(1, :, :) = MESH.NORMALS(1, :, :)./reshape(MESH.AREA, 1, 4, []);
MESH.NORMALS(2, :, :) = MESH.NORMALS(2, :, :)./reshape(MESH.AREA, 1, 4, []);
MESH.NORMALS(3, :, :) = MESH.NORMALS(3, :, :)./reshape(MESH.AREA, 1, 4, []);




id = MESH.NEIGHBOURS ==0;
MESH.Nx(id) = 0;
MESH.Ny(id) = 0;
MESH.Nz(id) = 0;

%% TESTS
return;
%1 for FCENTER & NORMALS
figure(3);
clf;
hold on;
dnodes = MESH.NODES(:,idx(:, 100));
trisurf(faces', dnodes(1,:), dnodes(2, :),dnodes(3,:), zeros(1, 4));
quiver3(MESH.FCENTER(1, :, 100),MESH.FCENTER(2, :, 100), MESH.FCENTER(3, :, 100), ...
     MESH.NORMALS(1, :, 100), MESH.NORMALS(2, :, 100), MESH.NORMALS(3, :, 100));
 axis equal, axis tight;
view(3);
end

