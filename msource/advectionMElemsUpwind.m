function [ T ] = advectionMElemsUpwind( MESH,T, dt, vx,vy)

dot_pro = vx.*MESH.Nx+vy.*MESH.Ny;
idc_el = dot_pro < 0;
idx = MESH.idx0;
idx(idc_el) = MESH.NEIGHBOURS(idc_el);
flux = sum(T(idx).*dot_pro);
% update
T = T- dt./MESH.AREA.*flux;
end