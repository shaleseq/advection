function [ T ] = advectionMNodesUpwind( MESH,T, dt, vx,vy)
%
%

%MESH.NEIGHBOURS = MESH.ELEMS([2 3 1]',:); NEIGHBOURS are for ELEMS

dot_pro = vx.*MESH.Nx+vy.*MESH.Ny;

idc_el = dot_pro < 0;

idp = MESH.ELEMS;
idn = MESH.NEIGHBOURS;

idp(idc_el) = MESH.NEIGHBOURS(idc_el);
idn(idc_el) = MESH.ELEMS(idc_el);


idx = [idp idn];

dot_pro = dot_pro .* T(idp); % upwind

dot_pro = [abs(dot_pro) -abs(dot_pro)]; %  +/- flux
flux = accumarray(idx(:), dot_pro(:));

% update

T = T- dt./MESH.AREA'.*flux';
end