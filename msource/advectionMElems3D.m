function [ T ] = advectionMElems3D( MESH,T, dt, vx,vy,vz, method)
if nargin < 6
    method =  'upwind';
end
switch method
    case 'upwind'
        T = advectionMElemsUpwind3D(MESH,T,dt,vx,vy,vz);
    case 'laxwen'
       T = advectionMElemsLaxWen3D(MESH,T,dt,vx,vy,vz);
    otherwise
           display('avaible methods upwind, laxwen');
           T=[];
end
end