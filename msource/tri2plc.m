function facetlist = tri2plc( tri )
    N = length(tri);
    facetlist = repmat(struct('polygonlist', {{int32([0 0 0])}}), 1, N);
    for i=1:N
        facetlist(i).polygonlist{1}(:) = int32(tri(:,i));
    end
end

