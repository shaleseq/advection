function y = fluxLimiter(x, method)
% limiter for flux
    function y=fl(z)
        switch method
            case 'upwind'
                y=0;
            case 'laxwen'
                y=1;
            case 'minmod'
                y=max(0,min(1,z));
            case 'superbee'
                y=max([0,min(1,2*z),min(2,z)]);
            otherwise
                y = 0;
        end
    end
y=arrayfun(@fl,x);
end

