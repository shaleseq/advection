function y = correct( A, phi, r, u)
%%Params
N=length(A);
y=zeros(N, 1);
f1=[2:N 1];
f2=[3:N 1 2];
b1=[N 1:N-1];
b2=[N-1 N 1:N-2];

%% Zalesak
A(A.*(phi(f1)-phi) < 0 | A.*(phi(f2)-phi(f1)) < 0 | A.*(phi-phi(b1)) < 0)=0;%antidiffusive to zero
mat1= [phi(b1), phi, phi(f1), u(f1), u, u(b1)];
phiMax=max(mat1,[],2);
phiMin=min(mat1,[],2);

Pp=max(zeros(N,1),A(b1))-min(zeros(N,1),A);
Qp=(phiMax-phi).*r;
Rp=arrayfun(@gRp, Qp, Pp);

Pm=max(zeros(N,1),A)-min(zeros(N,1),A(b1));
Qm=(phi-phiMin).*r;
Rm=arrayfun(@gRm, Qm, Pm);

C=(A>=0).*min(Rp(f1),Rm)+(A<0).*min(Rp,Rm(f1));

y=C.*A;
    function y=gRp(q,p)
        if(p >0)
            y=min(1, q/p);
        else
            y=0;
        end
        
    end
    function y=gRm(q,p)
        if(p >0)
            y=min(1, q/p);
        else
            y=0;
        end
        
    end
end

