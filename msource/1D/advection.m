function [ u ] = advection( x0, y0, t, v, method)
%function return numeric solution of advection equation 
%args x0-points,y0-initial values, t-time, v-velocity, method default 'laxwen'.

  if nargin < 5
    method =  'laxwen';
  end
   Nt = length(t);
   Mx = length(x0);
   u = zeros(Mx, Nt);
   u(:, 1) = y0;
   dx = x0(2)-x0(1);
   dt = t(2) - t(1);
   r=v./2*dt./dx;
   N = Nt - 1;
   r2 = 2*r;
    
    switch method
        case 'ftcs'
            for i = 1:N
                u(:, i+1) = u(:, i) -r'.*(u([2:end 1], i) - u([end 1:end-1], i));
            end
        case 'lax'
            for i = 1:N
                u(:, i+1) = 0.5*(u([2:end 1], i) + u([end 1:end-1], i) ) - r'.*(u([2:end 1], i) - u([end 1:end-1], i));
            end
        case 'middle'
            %upwind for second point
            %u(:, 2) = 0.5*(u([2:end 1], 1) + u([end 1:end-1], 1) ) - r(1)*(u([2:end 1], 1) - u([end 1:end-1], 1));
            u(:, 2) = u(:, 1) -r2'.*(u(:,1) - u([end 1:end-1], 1));
            for i = 2 : N     
                u(:, i+1) = u(:, i-1) - r2'.* (u([2:end 1], i) - u([end 1:end-1], i)); 
            end
        case 'laxwen'      
            for i = 1 : N   
       u(:, i+1) = u(:, i) -r2' .* (0.5*(u([2:end 1], i)-u([end 1:end-1], i))+...
            r' .*(2*u(:,i) - u([end 1:end-1], i)-u([2:end 1], i)));
            end
        case 'upwind'
            for i = 1:N
                u(:, i+1) = u(:, i) -r2'.*(u(:,i) - u([end 1:end-1], i));
            end
        case 'upwind2R'
            for i = 1:N
                u(:, i+1) = u(:, i) -r'.*(3*u(:,i) - 4*u([end 1:end-1], i) + u([end-1 end 1:end-2],i));
            end
        otherwise
            display('avaible methods: ftcs, lax, middle, laxwen, upwind, upwind2R');
            u = [];
            return;
    end

end



