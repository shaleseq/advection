function [ u ] = fluxCorrectorAdvection( x0, y0, t, v, method)
%function return numeric solution of advection equation
%args x0-points,y0-initial values, t-time, v-velocity, method default upwindC.
%avaible methods: upwindC, laxC

if nargin < 5
    method =  'upwindC';
end
Nt = length(t);
Mx = length(x0);
u = zeros(Mx, Nt);
u(:, 1) = y0;
dx = x0(2)-x0(1);
dt = t(2) - t(1);
v=v';
r=v./2*dt./dx;
N = Nt - 1;
r2 = 2*r;
R=(dt./dx)';
f=[2:Mx 1];
b=[Mx 1:Mx-1];
b2=[Mx-1 Mx 1:Mx-2];
switch method
   
    case 'upwindC'
        for i=1:N
            %% Calculate upwind
            f1=0.5*v.*(u(f,i)+u(:,i)) - 0.5*abs(v).*(u(f,i)-u(:,i));
            %% Calculate LaxWen
            f2=0.5*v.*(u(:,i)+u(f,i))-v.*r.*(u(f,i)-u(:,i));
            A=f2-f1;%A(i)=A_{i+0.5}
            %% Calculate value
            phi=u(:,i) -R.*([f1(1)-f1(end);diff(f1)]);%1R solve
            %% Aplay correction
            
            Ac=correct(A,phi,1./R, u(:, i));%%correct solution
            u(:, i+1) = phi-R.*([Ac(1)-Ac(end);diff(Ac)]);%apply correction
        end
        
    case 'laxC'
        for i=1:N
            %% Calculate Lax
            f1=0.5*v.*(u(f,i)+u(:,i))- (dx./(2*dt))'.*(u(f,i)-u(:,i));
            
            %% Calculate LaxWen
            f2=0.5*v*(u(:,i)+u(f,i))-v.*r.*(u(f,i)-u(:,i));
            A=f2-f1;%A(i)=A_{i+0.5}
            
            phi=u(:,i) -R.*([f1(1)-f1(end); diff(f1)]);
            Ac=correct(A,phi,1./R, u(:,i));
            u(:, i+1) = phi-R.*([Ac(1)-Ac(end); diff(Ac)]);
        end
    otherwise
        display('avaible methods: upwindC, laxC');
        u = 0;
        return;
end

end



