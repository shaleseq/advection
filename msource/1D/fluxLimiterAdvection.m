function [ u ] = fluxLimiterAdvection( x0, y0, t, v, method, limiter)
%function return numeric solution of advection equation
%args x0-points,y0-initial values, t-time, v-velocity, method only laxwen
%available limiters: upwind laxwen minmod superbee. 

if nargin < 5
    method =  'laxwen';
end
if nargin < 6
    limiter = 'superbee';
end
Nt = length(t);
Mx = length(x0);
u = zeros(Mx, Nt);
u(:, 1) = y0;
dx = diff(x0);
dx = [dx dx(end)];
dt = t(2) - t(1);
r=v/2*dt./dx;
N = Nt - 1;
r2 = 2*r;
f=[2:Mx 1];
b=[Mx 1:Mx-1];
b2=[Mx-1 Mx 1:Mx-2];
limiters = 'upwind laxwen minmod superbee';
if  isempty(strfind(limiters, limiter))
    u = 0;
    display('wrong limiter')
    display('avaible methods for limiter: upwind, laxwen, minmod, superbee');
    return;
end
switch method
    case 'laxwen'
        for i=1:N
            rp=(u(:,i)-u(b,i))./(u(f,i)-u(:,i));
            rp(isnan(rp)|isinf(rp))=0;
            cp=fluxLimiter(rp, limiter);
            %fp=u(:,i)+0.5*(1-abs(r2')).*(u(f,i)-u(:,i)).*cp;
            
            fh = 0.5*(u(f,i)+u(:,i)) - 0.5*r2'.*(u(f,i)-u(:,i));
            fp = u(:,i) + cp.*(fh-u(:,i));
            
            u(:,i+1)= u(:,i) - r2'.*(fp-fp(b));
            
        end
    otherwise
        u = 0;
        display('wrong method');
        display('avaible methods: laxwen');
        return;
end

end



