function y=bc(x,x0,xf)
    y=x;
    L=xf-x0;    
    y(y>xf) = y(y>xf) -floor((y(y>xf)-x0)/L)*L;
    y(y<x0) = y(y<x0) +floor(-(y(y<x0)-xf)/L)*L;
end