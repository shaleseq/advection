function [ y ] = advectionAnalytics( x0, y0, t, v,n, method)
%function return analytics solution of advection equation 
%args x0-points,y0-initial values, t-time, v-velocity, method default 'laxwen'.
    if nargin < 6
    method =  'laxwen';
    end
    dx = diff(x0);
    dx = [dx dx(end)];
    dt  = t(2)-t(1);
    [a, a0, b, k] = fourier(x0, y0);
    L = x0(end) - x0(1) + dx(end);
    x_check = linspace(0,L,length(x0)+1);
    x_check(end)=[];
    dx = dx(1:size(k));
    alpha = v*dt./dx;
  
    switch method
        case 'ftcs'
            lam  = ones(1, length(k)) - 1i*alpha.*sin(k.*dx);
        case 'lax'
            lam = cos(k.*dx)-1i*alpha.*sin(k.*dx);
        case 'middle'
            lam = sqrt(ones(1, length(k)) -(alpha.*sin(k.*dx)).^2) -1i*alpha.*sin(k.*dx);
        case 'laxwen'
            lam =  ones(1, length(k)) - 1i*alpha.*sin(k.*dx) - alpha.^2.*(ones(1, length(k))-cos(k.*dx));
        case 'upwind'
            lam = ones(1, length(k)) - alpha.*(ones(1, length(k)) - exp(-1i*k.*dx));
        case 'upwind2R'
            lam = ones(1, length(k)) - alpha./2.*(ones(1, length(k))* 3 - ones(1, length(k))*4.*exp(-1i*k.*dx) +exp(-2*1i*k.*dx));
        otherwise
            display('avaible methods: ftcs, lax, middle, laxwen, upwind, upwind2R');
            y = [];
            return;
    end
    
    y = a0+0*x0;
    phi = atan(imag(lam) ./ real(lam));
    for j = 1:length(k)-1
        y = y + a(j)*abs(lam(j)).^n*cos(2*pi*j*x_check/L + n*phi(j)) ...
            + b(j)*abs(lam(j)).^n*sin(2*pi*j*x_check/L+n*phi(j));
    end
    if mod(length(x0),2)==0
        y = y+real(a(end))*cos(2*pi*(j+1)*x_check/L +n*phi(j));
    end
    
    abs_lam = abs(lam);
    abs_min = min(abs_lam);
    abs_max = max(abs_lam);
    disp('summary of method');
    disp(method);
    if(abs_max > 1.0001)
      disp('this algorithm is unstable with factor');
      disp(abs_max);
    end
    if(abs_min < 0.9999)
       disp('this algorithm is diffuse with factor');
       disp(abs_min);
    end
    disp('average factor');
    average = mean(abs_lam);
    disp(average);
end

