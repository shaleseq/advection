function [a, a0, b, k] = fourier(x, y)
%function return fourier coefficients
f = fft(y);
m = length(y);
M = floor((m+1)/2);

a0=f(1)/m;
a = 2*real(f(2:M))/m;

if mod(length(x),2)==0
    a = [a f(M+1)/m];
    b = [-2*imag(f(2:M))/m 0];
else
    b = -2*imag(f(2:M))/m;
end

L   = x(end)-x(1);
dx  = x(2)-x(1);
k   = 2*pi.*(1:length(a))/(L+dx);