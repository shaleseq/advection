function [ T ] = advectionMElemsLaxWen( MESH,T, dt, vx,vy)
idx = MESH.idx0;
neigh = MESH.NEIGHBOURS;
dot_pro = vx.*MESH.Nx + vy.*MESH.Ny;%scaled by length
dot_pro2 = (vx.*MESH.NX).^2.*MESH.LENGTH + (vy.*MESH.NY).^2.*MESH.LENGTH;
dot_pro(neigh==0) = 0;
dot_pro2(neigh==0) = 0;
idc_el = dot_pro < 0;
idx(idc_el) = MESH.NEIGHBOURS(idc_el);
neigh(idc_el) = MESH.idx0(idc_el);
id = 1;
neigh(neigh==0) = id;%hack, when neigh=0 then dot_pro =0, so choose any id 
d = MESH.CENTER(:,idx) - MESH.CENTER(:,neigh);
d = sqrt(dot(d,d));
flux = sum((T(idx) + T(neigh))*0.5.*dot_pro) -...
    sum((T(neigh)-T(idx)).*dt.*dot_pro2*0.5./reshape(d, 3, []));
%flux = sum((T(idx) + T(neigh))*0.5.*dot_pro);
flux = sum(T(idx).*dot_pro);

% update
T = T- flux*dt./MESH.AREA;
end