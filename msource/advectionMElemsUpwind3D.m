function [ T ] = advectionMElemsUpwind3D( MESH,T, dt, vx,vy, vz)

dot_pro = vx.*MESH.Nx+vy.*MESH.Ny+vz.*MESH.Nz;
idc_el = dot_pro < 0;
idx = MESH.idx0;
idx(idc_el) = MESH.NEIGHBOURS(idc_el);

flux = sum(T(idx).*dot_pro);
% update
T = T- dt./MESH.VOL.*flux;
end