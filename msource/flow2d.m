function [Vel, p] = flow2d(MESH,BC,el_Mu,rhs)  

%% model info
nel  = MESH.info.nel;
ndim = MESH.info.ndim;
nnod = MESH.info.nnod;
ndof = ndim*nnod;
nedof = 14;
np = 3;
ar = polyarea(reshape(MESH.NODES(1,MESH.ELEMS(1:3,:)),3,nel),...
              reshape(MESH.NODES(2,MESH.ELEMS(1:3,:)),3,nel));
          
%% matrix computation
[A_all, Q_all, M] = mechanical_matrix_opt(MESH, el_Mu);

%% matrix assembly
% A matrix
opts_sparse.symmetric = 1;
opts_sparse.n_node_dof = 2;
A = msparse(MESH.ELEMS,A_all,opts_sparse, BC.periodic.map);

% Q matrix
mmessage('Q MATRIX ASSEMBLY:'); tstart=tic;
ELEM_DOF = zeros(nedof, nel,'int32');
ELEM_DOF(1:ndim:end,:) = ndim*(MESH.ELEMS-1)+1;
ELEM_DOF(2:ndim:end,:) = ndim*(MESH.ELEMS-1)+2;

Q_i = repmat(int32(1:nel*np),nedof,1);
Q_j = repmat(ELEM_DOF,np,1);
if ~isempty(BC.periodic.map)
    Q_j = BC.periodic.map(Q_j);
end
Q = sparse2(Q_i(:), Q_j(:), Q_all(:)); %"periodic" columns are added
mmessage('',tstart);

%% boundary conditions
BC = bc_eval(BC, MESH);
f = bc_apply_rhs(BC, MESH,  rhs, A);
A = bc_apply_mat(BC, A);

dof = [];
for ig=1:length(BC.dirichlet.grp)
    dof = [dof repmat(BC.dirichlet.dof(ig),1,BC.dirichlet.grp(ig))];
end
%bc_indx = uint32(BC.ndof)*([BC.dirichlet.ind{:}]-1)+uint32(dof);
bc_indx = BC.dirichlet.index;


if isempty(BC.periodic.map)
    Vel = zeros(BC.nnod*BC.ndof,1);
    Vel(bc_indx) = BC.dirichlet.val;
else
    Vel = zeros(BC.periodic.sdof,1);
    Vel(BC.periodic.map(bc_indx)) = BC.dirichlet.val;
end


% Q matrix
mmessage('BC - Q matrix:'); tstart = tic;
g = -Q*Vel;
if isempty(BC.periodic.map)
    Q(:,bc_indx) = [];
else
    Q(:,BC.periodic.map(bc_indx)) = [];
end

mmessage('',tstart);

%% factorize A
L = mfact(A);

%% pressure iterations (CG)
% solving inv(M)*Q*inv(A)*Q' = ...
%print_message('Pressure iterations:'); tstart = tic;
A = A + tril(A,-1)';

rhs = Q*mbfsubst(L,f)-g;
%
%rhs = Q*(A\f)-g;

pre = repmat(ar(:)'./el_Mu(:)',3,1);
%pre = repmat(ar(:)',3,1);
pre = pre(:);
invM = inv(M);


 [p, ~, relres, it, resvec] = pcg(@(x) Q*mbfsubst(L,Q'*x), rhs, 1e-12, 50, ...
     @(x)(reshape(invM*reshape(x,np,nel),np*nel,1))./pre);
 
%num2str(resvec,10)
%it
%p = pcg(@(x) Q*(A\(Q'*x)), rhs, 1e-14, 50, ...
%    @(x)(reshape(invM*reshape(x,np,nel),np*nel,1))./pre);

% display('no pressure iterations')
% p = 0*p;

if isempty(BC.periodic.map)
    Free = 1:BC.nnod*BC.ndof;
    Free(bc_indx) = [];
else
    Free = 1:BC.periodic.sdof;
    Free(BC.periodic.map(bc_indx)) = [];
end



Vel(Free) = mbfsubst(L,f-Q'*p);

%Vel = bc_apply_sol(Vel,BC);
if ~isempty(BC.periodic.map)
    Vel = Vel(BC.periodic.map);
end
Vel = reshape(Vel,2,MESH.info.nnod);