function MESH = prepareMeshSegments( MESH )


%% REORDER
% Gx = mean(reshape(MESH.NODES(1,MESH.ELEMS),3,[]));
% Gy = mean(reshape(MESH.NODES(2,MESH.ELEMS),3,[]));
% NOD = [Gx; Gy];
% perm = geom_order(NOD);
% MESH.ELEMS = MESH.ELEMS(:,perm);

reorder = 'metis';
opts.reordering_type = reorder;
MESH = mesh_reorder(MESH,opts);

EL = MESH.ELEMS;
nw = length(EL(1,:));
nh = length(EL(:,1));
nel = length(MESH.ELEMS);
EL2ED = zeros(1, nw*nh);
b=[2:nh 1];

ED = MESH.EDGES;
n=length(MESH.NODES(1,:));
nl = length(ED(1,:));
tED = uint64([ED ED([2,1],:)]);

tEL1 = reshape(EL, 1, nw*nh);
tEL2 = reshape(EL(b,:),1, nw*nh);
tEL = uint64([tEL1;tEL2]);

tED = tED(1,:)+(tED(2,:)-1)*n;
tEL = tEL(1,:)+(tEL(2,:)-1)*n;

[~,B,C]=intersect(tED,tEL);

EL2ED(C)=B;
EL2ED(EL2ED>nl)=-EL2ED(EL2ED>nl)+nl;
EL2ED=reshape(EL2ED, nh, nw);
EL2ED2 = EL2ED;
id = EL2ED < 0;
EL2ED2(id) = abs(EL2ED2(id))+nl;

ED2EL = zeros(1, nl*2);
ED2EL(EL2ED2) = reshape(repmat([1:nw], nh, 1),1,nh*nw);

neigh = EL2ED;
neigh(~id)=neigh(~id)+nl;
neigh=abs(neigh);
neigh=reshape(ED2EL(neigh(:)),nh,nw);
MESH.NEIGHBORS=int32(neigh);



%% NORMALS && CENTER

MESH.NORMALS = zeros(2, nl);
X = MESH.NODES(1, :);
Y = MESH.NODES(2, :);
lx1 = X(ED(2,:))-X(ED(1,:));
ly1 = Y(ED(2,:))-Y(ED(1,:));

MESH.NORMALS = getNorm(lx1, ly1);

MESH.LENGTH = zeros(1,nl);
MESH.LENGTH = sqrt(lx1.^2+ly1.^2);

MESH.CENTER = zeros(2,nel);
MESH.CENTER(1,:) = mean(reshape(MESH.NODES(1,MESH.ELEMS(1:3,:)),3,nel));
MESH.CENTER(2,:) = mean(reshape(MESH.NODES(2,MESH.ELEMS(1:3,:)),3,nel));
MESH.AREA = polyarea(reshape(MESH.NODES(1,MESH.ELEMS(1:3,:)),3,nel),...
    reshape(MESH.NODES(2,MESH.ELEMS(1:3,:)),3,nel));

MESH.SEGMENTS = zeros(2, n);
MESH.SEGMENTS = [(X(ED(2,:))+X(ED(1,:)))/2; (Y(ED(2,:))+Y(ED(1,:)))/2];

id = ED2EL ==0;
id = id(nl+1:end);
MESH.NORMALS(1,id) = 0;
MESH.NORMALS(2,id) = 0;
MESH.ED2EL=uint32(ED2EL);
MESH.Nx = MESH.NORMALS(1,:).*MESH.LENGTH;
MESH.Ny = MESH.NORMALS(2,:).*MESH.LENGTH;

end

