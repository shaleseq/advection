function T=tinterp(MESH, Tv, r, TR, opts, WS)
%function for linear interpolation
%MESH
%Tv-values in Nodes
%r-markers
%TR-map of ids of markers if already exists
%WS workspace
[TR, WS] = tsearch2(MESH.NODES, MESH.ELEMS(1:4, :), r, WS, TR, opts);
val = reshape(Tv(MESH.ELEMS(:,TR)), 4, []);
x=reshape(MESH.NODES(1,MESH.ELEMS(:,TR)),4,[]);
y=reshape(MESH.NODES(2,MESH.ELEMS(:,TR)),4,[]);
dx = repmat(r(1, :), 4, 1)-x;
dy = repmat(r(2,:),4 ,1)-y;
d = sqrt(dx.^2+dy.^2);

len = sum(d);
d = d./repmat(len, 4, 1);
T=sum(val.*d);

end

