function [ T ] = advectionMSegments( MESH,T, dt, vx,vy)
nl=length(MESH.EDGES(1,:));
n=length(MESH.CENTER);
ED2ELp = MESH.ED2EL(1:nl);
ED2ELn = MESH.ED2EL(nl+1:end);

dot_pro = vx.*MESH.Nx+vy.*MESH.Ny;
idp = dot_pro > 0;
idn = dot_pro < 0;
dot_pro(idp) = dot_pro(idp).*T(ED2ELp(idp));
dot_pro(idn) = dot_pro(idn).*T(ED2ELn(idn));

flux = zeros(1,n);
idn = ED2ELn ~= 0;

fp = accumarray(ED2ELp',dot_pro');
flux(1:length(fp)) = fp;
flux = flux + (accumarray(ED2ELn(idn)', -dot_pro(idn)'))';
T = T- dt./MESH.AREA.*flux;
end

